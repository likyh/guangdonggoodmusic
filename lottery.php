<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta  name="keywords" content="广东好歌，广东好歌官网，广东好歌选秀大赛广东好歌报名网站">
<meta name="description" content="广东好歌是由雀巢五羊雪糕倾心打造的大型粤语歌曲选秀大赛，3月19日开始海选报名，了解更多请访问网络报名唯一指定网站。" >
<title>广东好歌选秀大赛官方网站，网络报名唯一指定网站</title>
<link href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="style/lottery.css"/>
<link rel="stylesheet" type="text/css" href="style/lotteryemoji.css"/>
<script type="text/javascript" src="script/reset.js"></script>
<script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="script/layer.min.js"></script>
<script type="text/javascript" src="script/getCookie.js"></script>
<script type="text/javascript">
    pageName="lottery.php";
    pn="works";
    if(getCookie('userOpenId')!=""){
        userId=getCookie('userOpenId');
    }else{
        userId="";
    }
    dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#draw').click(function() {
		$('#modal-unique').removeClass('util-hide');
		var $container = $('#modal-unique').find('.container');
		$container.css('top', ($(window).height()-$container.height())/3);
        dataLayer.push({'event':'event','cat':'抽奖','act':'参与抽奖','lbl':''});
    });
	$('#form-draw').submit(function(e) {
		e.preventDefault();
        dataLayer.push({'event':'event','cat':'抽奖','act':'确认提交','lbl':''});
        var $button = $(this).find('input[type=submit], .close');
		code = $(this).find('[name=code]').val();
		$(this).find('[name=code]').val("");
		if (code == '') {
			alert('请输入编码');
		}else {
			$button.prop('disabled', true).addClass('disabled');
            $.ajax({
                url: 'http://wuyang.wc.m0.hk/api/award?callback=?',
                data: {code: code,"source":"pc"},
                success: function(data) {
                    if (data.err) {
                        alert(data.err);
                        $button.prop('disabled', false).removeClass('disabled');
                    }else if (data.type==1) {
                        dataLayer.push({'event':'event','cat':'抽奖','act':'表情中奖','lbl':''});
                        var imageUrl=data.prize.image;
                        $layer = $.layer({
                            type: 1,
                            title: false,
                            closeBtn: false,
                            border : [0, 0, '#666', true],
                            bgcolor :null,
                            area: ['515px','675px'],
                            page: {
                                dom : '.site-emoji'
                            },
                            success: function(){
                                layer.shift('top',500);
                            }
                        });
                        $(".site-emoji").find("#emoji").attr("src",imageUrl);
                        $(".site-emoji").find("#save").attr("href",imageUrl);
                        $(".site-emoji").find("#save").attr("download",imageUrl);
                    }else{
                        dataLayer.push({'event':'event','cat':'抽奖','act':'实物中奖','lbl':''});
                        alert("恭喜，您抽中了"+data.prize.name+"，请完善您的信息。");
                        $layer = $.layer({
                            type: 1,
                            title: false,
                            closeBtn: false,
                            border : [0, 0, '#666', true],
                            bgcolor :null,
                            area: ['668px','799px'],
                            page: {
                                dom : '.site-gift'
                            },
                            success: function(){
                                dataLayer.push({'event':'page','branch':'抽奖','section':'实物中奖','pname':''});
                                layer.shift('top',500);
                            }
                        });
                    }
                    $button.prop('disabled', false).removeClass('disabled');
                },
                error:function(data){
                    alert("抱歉，网络忙，请重试");
                },
                dataType: "jsonp"
            });
        }
    });
    $('#form-gift').submit(function(e) {
        e.preventDefault();
        dataLayer.push({'event':'event','cat':'抽奖','act':'提交信息','lbl':''});
        var realname = $(this).find('[name=realname]').val();
        var phone = $(this).find('[name=phone]').val();
        var wechat = $(this).find('[name=wechat]').val();
        var address = $(this).find('[name=address]').val();
        $.ajax({
            url: 'http://wuyang.wc.m0.hk/api/savetake?callback=?',
            data: {code: code, realname: realname,phone: phone,wechat: wechat,address: address},
            success: function(data) {
                alert("感谢您的参与，您的信息已经记录，我们将会尽快发回，也请您保管好您的编码");
                layer.close($layer);
            },
            error:function(data){
                alert("抱歉，网络忙，请重试");
            },
            dataType: "jsonp"
        });
    });
	$('#modal-unique .close').click(function(e) {
		e.preventDefault();
		$("#modal-unique").find('[name=code]').val("");
		if (!$(this).hasClass('disabled')) {
			$('#modal-unique').addClass('util-hide');
		}
	});

    $(".site-emoji").find("#close").click(function(){
        layer.close($layer);
        return false;
    });
});	
	

</script>
</head>

<body>
<div class="site-home">
<div class="lottery">
<a id="back" href="index.php">返回首页</a>
	<div class="lucky">
		<a id="draw" class="draw" href="javascript:void(0)"></a>
	</div>
 </div>   
	<section class="summary">
		<div class="title">活动规则</div>
		<div class="content">
			<p>1.&nbsp;在活动期间，购买五羊促销装甜筒，从甜筒盖下获得编码。</p>
			<p>2.&nbsp;一个编码一次抽奖机会。</p>
			<h4>声明：</h4>
			<p>1.&nbsp;在法律允许范围内，主办方对本次活动拥有解释权。</p>
			<p>2.&nbsp;所有奖品图片仅供参考，奖品均以收到实物为准。</p>
		</div>
	</section>
	<h3 class="hot">抽奖总共四轮，第四轮火热进行中~<br />活动时间：6月20日-7月20日</h3>
	<table class="info">
		<tr>
			<th>奖  品</th>
			<th>奖  品  价  值</th>
			<th>数  量</th>
		</tr>
		<tr class="single">
			<td>苹果iPad Air*</td>
			<td>3588元</td>
			<td>5</td>
		</tr>
		<tr class="even">
			<td>五羊T恤</td>
			<td>100元</td>
			<td>100</td>
		</tr>
		<tr class="even">
			<td>Sony MDR-EX450耳机</td>
			<td>299元</td>
			<td>10</td>
		</tr>
        <tr class="even">
			<td>小米移动电源</td>
			<td>49元</td>
			<td>100</td>
		</tr>
        <tr class="even">
			<td>邓紫棋CD</td>
			<td>100元</td>
			<td>10</td>
		</tr>
        <tr class="even">
			<td>五羊雪糕抱枕</td>
			<td>50元</td>
			<td>100</td>
		</tr>
        <tr class="even">
			<td>五羊U盘8GB</td>
			<td>60元</td>
			<td>100</td>
		</tr>
	</table>
	<p class="tips">*苹果公司非联合活动商亦非本次活动的赞助商</p>
</div>

<section id="modal-unique" class="modal-unique util-hide">
	<form id="form-draw" class="container" action="http://wuyang.wc.m0.hk/site/award">
    <a class="close" href="#"></a>
		<h1>请输入编码开始抽奖!<br />iPad好礼等你赢</h1>
        <h2>从促销装甜筒盖下获得编码。</h2>
		<p><input type="text" name="code" /></p>
		<p><input type="submit" value="确认抽奖" /></p>
		
	</form>
</section>
<div class="site-emoji hide">
    <a id="close" href="#"></a>
    <h3>恭喜你中奖了！</h3>
    <h4>你赢得了粤味表情，马上保存下来吧！</h4>
	<img id="emoji" src="" alt="" />
	
	<a id="save" href=" " download="" class="save"></a>
</div>

<div id="menu" class="site-gift" style="display: none">
<form action="" method="post" class="site-take" id="form-gift">
	<div class="control">
		<div class="label">姓名</div>
		<input type="text" name="realname" value="" />
	</div>
	<div class="control">
		<div class="label">手机号码</div>
		<input type="text" name="phone" value="" placeholder="例如：159xxxxxxxx" />
	</div>
	<div class="control">
		<div class="label">寄送地址</div>
		<input type="text" name="address" value="" />
	</div>
	<div class="control">
		<div class="label">微信号</div>
		<input type="text" name="wechat" value="" placeholder="选填" />
	</div>
	<div class="submit">
		<p>（请确保您填写的信息真实有效）</p>
		<input type="submit" class="btn" value="确认提交" />
	</div>
</form>
</div>
</body>
</html>