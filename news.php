<?php
include "check.php"; 
 include_once "lib/news.class.php";
 $gr=new GameReport();
 $pagesize=4;
 $sql="select count(*) from news";
 $rows=$gr->getRows($sql);//一共有多少条记录
 $pages=$gr->getPages($rows,$pagesize);//一共有多少页
 $currentpage=isset($_GET['page'])?(int)$_GET['page']:1;
 if($currentpage<1){
  $currentpage=1;
 }else if($currentpage>$pages){
  $currentpage=$pages;
 }
 $prevPage=$currentpage-1>0?$currentpage-1:1;
 $nextPage=$currentpage+1<=$pages?$currentpage+1:$pages;
  
 $pageresultset=array();
 $pageresultset=$gr->paging($currentpage,$pagesize);  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
	<meta charset="utf-8">
    <meta  name="keywords" content="赛事报道">
	<meta name="description" content="广东好歌选秀大赛官方网站为您提供广东好歌赛事全程报道，让您第一时间看到广东好歌赛事的最近进展和赛事热点。" >
	<title>【赛事报道】选秀赛事报道,大赛赛事报道-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/news.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="news.php";
		pn="news";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/layer.min.js"></script>    
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
  <div id="header">
  	<div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter"><?php include 'part/personalCenter.php'; ?></div>
    <div id="title">
    	<div class="button"><?php include_once "part/navigation.php";?></div>
    </div>  
  </div>
    <div id="content" class="news">
    	<div id="newcontent">
      <?php foreach($pageresultset as $result){?>  
        <div class="newsItem">
            <a href="newsDetail.php?newsId=<?php echo $result['id']?>" onclick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'图片'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"><img src="<?php echo $result['picurl'];?>" alt="标题"></a>
            <div class="textarea">
            <h3><a href="newsDetail.php?newsId=<?php echo $result['id']?>" onclick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'标题'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">
			 <?php $title=$result['title']; 
				if(mb_strlen($title,"utf8")>20){			
				 echo mb_substr(strip_tags($title),0,20,"utf-8")."...";
				}else{
				 echo $title;
				}?></a></h3>
            <div id="time"><?php echo date('Y-m-d',strtotime($result['create_time']))?></div>
            <p><?php echo mb_substr(strip_tags($result['content']),0,100,'utf-8');?></p>
            <a href="newsDetail.php?newsId=<?php echo $result['id']?>" onclick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'查看更多'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">&gt;&gt;查看详情</a>
            </div>
        </div>
      <?php } ?> 
      </div>
      
        <div id="page">
            <a href="news.php?page=<?php echo $prevPage; ?>" >&lt;</a>
            <?php for($i=1;$i<=$pages;$i++){?>
            <a href="news.php?page=<?php echo $i; ?>" class="<?php if($currentpage==$i) echo "page_active"; ?>"><?php echo $i; ?></a>
            <?php } ?>
            <a href="news.php?page=<?php echo $nextPage; ?>">&gt;</a>
        </div>
  
    </div>
    <?php include_once "part/footer.php"; ?>
</div>

</body>
</html>
