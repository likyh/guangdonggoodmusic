<?php
include "check.php";
?><html>
<head>
    <meta charset="utf-8" name="keywords" content="广东好歌评委">
	<title>【评委构成】-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/activityIntro.css"/>
    <link rel="stylesheet" type="text/css" href="style/act.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="pingwei.php";
		pn="activity";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/activity.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
  <div id="header">
   <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter">
      <?php include 'part/personalCenter.php'; ?>
    </div>
    <div id="title">
      <div class="button"><?php include_once "part/navigation.php";?></div>
    </div>
  </div>  
  <div id="content" class="activityIntro">
      <div id="share">
          分享活动到：
          <a target="_blank" class="bds_tsina" title="分享到新浪微博" href="http://v.t.sina.com.cn/share/share.php?url=http%3a%2f%2fgdhg.kugou.com%2fpingwei.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'新浪微博'});"><img src="style/images/sinaimg.png"/></a>
            <a target="_blank" class="bds_renren" title="分享到人人网" href="http://widget.renren.com/dialog/share?resourceUrl=http://gdhg.kugou.com/shuoming.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=广东好歌&content=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'人人'});"><img src="style/images/renren.png"/></a>
            <a target="_blank" class="bds_douban" title="分享到豆瓣网" href="http://www.douban.com/recommend/?url=http%3a%2f%2fgdhg.kugou.com%2fpingwei.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'豆瓣'});"><img src="style/images/douban.png"/></a>
            <a target="_blank" class="bds_tengxun" title="分享到腾讯微博" href="http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fgdhg.kugou.com%2Fpingwei.php%3Futm_source%3Dsocialshare%26utm_medium%3Dtencent%26utm_campaign%3D5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'腾讯微博'});"><img src="style/images/tengxun.png"/></a>
      </div>
    <div class="navigationButton2">
       <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
    </div>
    <article> 
      <!--评委构成--> 
      <img src="style/images/navline2.png"/>
      <div id="judgeCon">
        <div id="judgeList">
          <ul>
            <li><img src="style/images/sdsy.jpg"/><span>东山少爷</span><br>
              广州知名歌手
              <p>用一个老广州著名的称号，唱出广州人被遗忘的往事</p>
            </li>
            <li><img src="style/images/cby.jpg"><span>陈柏宇 Jason Chen</span><br>
              香港著名歌手
              <p>斩获"全球华语歌曲排行榜"、"新城劲爆颁奖礼"、"全球华语歌曲排行<br>
                榜"等颁奖礼多项大奖 </p>
            </li>
            <li><img src="style/images/ss.jpg"/><span>三少</span>
              <p>华语乐坛最成功的独立流行乐队—与非门成员</p>
            </li>
            <li><img src="style/images/chr.jpg"/><span>陈浩然  Edward Chan</span> 香港著名音乐制作人,演唱会总监，
              <p>曾与谭咏麟、钟镇涛、梅艳芳、张学友、郭富城、陈奕迅等大牌艺人合作</p>
            </li>
          </ul>
        </div>
      </div>
    </article>
  </div>
  
</div>
<?php include_once "part/footer.php"; ?>
<!--END #container-->
</body>
</html>
