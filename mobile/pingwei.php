<?php
include "check.php";
?><!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8" name="keywords" content="广东好歌评委">
	<title>【评委构成】-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
	<meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/activity.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
	<script type="text/javascript">
        pageName="pingwei.php";
		pn="activity";
        if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
    <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
<?php include_once "header.php"; ?>
  <div id="activity">
    <div class="navigationButton2">
       <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
    </div>
    <article> 
      <!--评委构成--> 
      <img src="images/line2.png"/>
      <div id="judgeCon">
        <div id="judgeList">
          <ul>
            <li>
            <img src="images/sdsy.jpg"/>
            <p>
                <br><span>东山少爷</span>
                <br><br>
               
                广州知名歌手<br>用一个老广州著名的称号，<br>唱出广州人被遗忘的往事
            </p>
            </li>
            
            
            <li><img src="images/cby.jpg"/>
            <p>
                <span>陈柏宇 Jason Chen</span>
                <br><br>
                
                香港著名歌手<br>斩获"全球华语歌曲排榜"、<br>"新城劲爆颁奖礼"、"全球<br>华语歌曲排行
                榜"等颁奖礼多项大奖
            </p>
            </li>
            
            <li>
            
            <img src="images/ss.jpg"/>
            <p>
                <br><br><span>三少</span>
                <br><br>
                
              华语乐坛最成功的独立流行乐队<br>—与非门成员
            </p>
            </li>
            
            <li><img src="images/chr.jpg"/>
            <p>
                <br><span>陈浩然  Edward Chan</span> 
                <br><br>
                香港著名音乐制作人,演唱会总监,<br>
              曾与谭咏麟、钟镇涛、梅艳芳、张学友、<br>郭富城、陈奕迅等大牌艺人合作
            </p>
            </li>
          </ul>
        </div>
      </div>
    </article>
  </div>
</div>
<!--END #container-->
</body>
</html>
