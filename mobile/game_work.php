﻿<?php include "check.php";
  include_once "../lib/game_work.class.php";
  include_once "../lib/data.class.php";
  include_once "../lib/imageURL.php";
  $data=new Data();
  $imgUrl=new ImageUrl;
  $maxPeriod=$data->getPeriod();
  if(isset($_GET['period'])&&$_GET['period']<=5&&$_GET['period']>=1){
    $period=$_GET['period'];
  }else{$period=$maxPeriod;} 
  $gw=new GameWork();
  $resultset=$gw->getEntriesByPeriod($period);
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8" />
     <meta  name="keywords" content="参赛作品，参赛作品大全，试听参赛歌曲">
	<meta name="description" content="广东好歌选秀大赛官方网站参赛作品频道为您提供最全的广东好歌参赛作品，试听广东好歌参赛作品，免费试听广东好歌参赛作品。" >
	<title>【参赛作品】参赛作品大全，试听参赛歌曲-广东好歌选秀大赛官方网站</title>
     <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
     <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/gameWorkSearch.js"></script>
    <script type="text/javascript" src="js/layer.min.js"></script>
    <script type="text/javascript" src="js/vote.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="game_work.php";
		pn="music";
		if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
    <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
 <!--BEGIN #container-->
<div id="container">
    <?php include_once "header.php"; ?>
    <div id="main" class="gameWork">
    <div id="game_progress">
     <ul>
       <li class="first"><span>3月19日</span><a href="game_work.php?period=1" <?php if($maxPeriod<1){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";} ?> class="<?php echo $period==1?"periodActive":""; ?>"   onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'海选'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">海选</a></li>
       
       <li class="second"><span>4月21日</span><a href="game_work.php?period=2" <?php if($maxPeriod<2){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";} ?> class="<?php echo $period==2?"periodActive":""; ?>"   onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'50进25强'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">50进25强</a></li>
       
       <li class="third"><span>5月19日</span><a href="game_work.php?period=3" <?php if($maxPeriod<3){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";} ?> class="<?php echo $period==3?"periodActive":""; ?>"   onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'25进15强'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">25进15强</a></li>
       <li class="fourth"><span>6月18日</span><a href="game_work.php?period=4" <?php if($maxPeriod<4){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";} ?> class="<?php echo $period==4?"periodActive":""; ?>"   onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'复活赛'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">复活赛</a></li>
       <li class="end"><span>7月19日</span><a href="game_work.php?period=5" <?php if($maxPeriod<5){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";} ?> class="<?php echo $period==5?"periodActive":""; ?>"   onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'总决赛'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">总决赛</a></li>
     </ul>
    </div>
    
    <div id="search_work">
      <input name="" type="text" id="projectInput" placeholder="作品名/选手名"/>
      <a id="projectSubmit" href="#" data-period="<?php echo $period;?>" ></a>   
    </div>
    
    <p class="tips"><span class="tip">温馨提示：</span>如果你想查看往期权全部作品请到PC版活动网站查看</p>
    
    <div id="game_works">
     <?php foreach($resultset as $result){?>
     
      <input type="hidden" value="<?php echo $result['id']?> " id="m_id"/>
      <div id="game_work">
       <a href="comment.php?musicId=<?php echo $result['id']?>&period=<?php echo $period;?>" ><img src="<?php $pic_url=$result['pic_url']; $picurl=$imgUrl->changeUrl($pic_url,"_238_119");echo $picurl; ?>" /></a>
      <div class="game_work_title">
        <p class="song_name"><a href="comment.php?musicId=<?php echo $result['id']?>&period=<?php echo $period;?>" ><?php $title=$result['title']; if(mb_strlen($title,"utf8")>8){echo mb_substr(strip_tags($title),0,8,"utf8")."...";}else{echo $title;}?></a></p>
        <p class="singername"><?php echo $result['singer']?></p>        
        <div id="vote">
        <span class="vote_number"><?php echo $result['vote']?></span>
        <span>
        <?php if($maxPeriod==$period){?>
         <a href="#" class="voteButton" data-period="<?php echo $period; ?>" data-id="<?php echo $result['id']?>">投票</a>
        <?php }else{?><a class="nagitiveVoteButton" href="#">票数</a> <?php }?>
          
        </span>


        </div>
    <!-- vote action没有写-->    
      </div>
     </div>
     <?php } ?>     
    </div>
    </div>
    <div id="float" class="show">
        <a id="close" href="#"></a>
        <div class="message">
            <h4>提交中</h4>
            <p>正在提交您的请求，请稍等……</p>
        </div>
        <div class="uniqueCode">
           <h4>您也可以直接输入产品编码为选手投票！</h4>
           <p>购买五羊甜筒,从甜筒盖下获取编码！</p>
            <input type="text" id="uniqueCodeInput"/>
            <a href="#"  data-period="<?php echo $period; ?>"  id="sureVote">确认投票</a>
            <div class="qcode">编码还可以在微信抽取惊喜大礼！<br><a href="http://t.cn/8sl7Yk0">马上抽奖&gt;</a></div>
        </div>
    </div>
</div>
    
</body>
</html>
