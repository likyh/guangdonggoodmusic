<?php
   include "check.php"; 
   include_once "../lib/data.class.php";
   if(isset($_GET['newsId'])&&!empty($_GET['newsId'])){
     $newsId=$_GET['newsId'];
   }else{
     $newsId=1;
   }
   $data=new Data();
   $result=$data->getNewsListDetailed($newsId);
   $otherInfo=$data->getOtherDetail($newsId);
?><!DOCTYPE html >
<html >
<head>
    <meta charset="utf-8" />
    <meta  name="keywords" content="<?php echo $result['title'];?>">
	<meta name="description" content="<?php echo mb_substr(strip_tags($result['content']),0,200,'utf8');?>" >
	<title>报道详情标题-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
    <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="newsDetail.php";
		pn="news";
		params=window.location.search;
		pageName+=params;
        if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
    <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
 <div id="container">
    <?php include_once "header.php"; ?>
   <div id="content" class="newsDetail">
        <div id="article">
            <div class="newsTitle">
                <h1> 
                 <?php $title=$result['title']; echo $title;?>
                </h1>
                <h4>时间:<span id="time"><?php echo date('Y-m-d',strtotime($result['create_time']));?></span><a href="news.php">返回列表&gt;&gt;</a></h4>
            </div>
            <div class="mainDetail">
            <?php if(isset($result['video_url'])&&!empty($result['video_url'])){ ?>    
                <video width="320" height="240" controls> 
      				<source src="http://gdhg.kugou.com/<?php echo $result['video_url'];?>" type="video/mp4"> 
      				<object data="" width="320" height="240">     
        				<embed width="320" height="240" src="<?php echo $result['video_url'];?>"> 
      				</object> 
    			</video>
           <?php }else if(isset($result['picurl'])&&!empty($result['picurl'])){ ?>    
            	<img src="http://gdhg.kugou.com/<?php echo $result['picurl'];?>"  alt="image"/>
            <?php } ?>
            
                <p><?php echo $result['content']?></p>
            </div>
        </div>
        
        <div id="share">分享到：
         <?php include_once "part/newsShareLink.php";?>
        </div>
        
        <div id="moreNews">
          <h2>更多新闻</h2>
          <div class="newsItem">
        <ul>
          <?php foreach($otherInfo as $result){?>
           <li>
            <a href="newsDetail.php?newsId=<?php echo $result['id']; ?>">
            <img src="http://gdhg.kugou.com/<?php echo $result['picurl']; ?>" alt="标题">
            <h3>
            <?php $title=$result['title']; if(mb_strlen($title,"utf8")>20){echo mb_substr(strip_tags($title),0,20,"utf8")."...";}else{echo $title;}?>
            </h3>
            <h4>时间:<span id="time"><?php echo $result['create_time']; ?></span></h4>
            <p><?php echo mb_substr(strip_tags($result['content']),0,40,'utf8');?></p>
            <h5>查看详情&gt;</h5>
            </a>
            </li>
          <?php }?>
         </ul>
        </div>  
            
        </div>
    </div>
</div>
</body>
</html>

