$(document).ready(function(){
    $("#kv li:last").hide();

    var startX,ifMove=false,first=true;

    document.getElementById("kv").addEventListener("touchstart", touchStart, false);
    document.getElementById("kv").addEventListener("touchmove", touchMove, false);
//    document.getElementById("kv").addEventListener("touchend", touchEnd, false);
    function touchStart(event){
        var touch = event.touches[0];
        if(!startX){
            $("#kv li:first").css({left:"0"});
            $("#kv li:last").css({left:"100%"});
            $("#kv li").show();
        }
        startX = touch.pageX;
        ifMove=true;
    }
    function touchMove(event){
        var touch = event.touches[0];
        if(ifMove){
            if(startX-touch.pageX>15){
                $("#kv li:first").animate({left:"-100%"},"slow");
                $("#kv li:last").animate({left:"0"},"slow");
                $(".promptumenu_nav a:first").removeClass("active");
                $(".promptumenu_nav a:last").addClass("active");
                ifMove=false;
            }else if(startX-touch.pageX<-15){
                $("#kv li:first").animate({left:"0"},"slow");
                $("#kv li:last").animate({left:"100%"},"slow");
                $(".promptumenu_nav a:first").addClass("active");
                $(".promptumenu_nav a:last").removeClass("active");
                ifMove=false;
            }
        }
    }
//    function touchEnd(event){
//        var touch = event.touches[0];
//        alert(startX-touch.pageX);
//        if(startX-touch.pageX>20){
//            $("#kv li").toggle("slow");
//        }
//    }
});


