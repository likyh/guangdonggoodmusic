$(document).ready(function(){ 
  var i=0;
  $('#workDetailComment').addClass("workDetail");
  $("#textCount").text('*至少5个字至多50个字');
  /*显示选手信息不显示评论信息 */
  $('#workComment').css("display","block");
  $('#commentSubmit').css("display","none");  
  checkCookie();
  /* 判断输入框内字的个数 */
  $('#TextArea1').keyup(function(){  			 
    var curLength=$("#TextArea1").val().length;	
	var maxlength=50;
	var minlength=5;
    if(curLength>maxlength){
      var words=$("#TextArea1").val().length-maxlength;
      $("#textCount").text('超出'+words+'个字');
      $("#textCount").css("color","red");
    }else if(curLength<minlength){
     /* var words=maxlength-$("#TextArea1").val().length;	*/
      $("#textCount").text('少于5个字');
	  $("#textCount").css("color","red");
    }else{
	   $("#textCount").css("color","black");	
      var words=maxlength-$("#TextArea1").val().length;	
      $("#textCount").text('剩余'+words+'个字');
    }  
  });

  /*var userId=$('#userId').val();*///用cookie值来获取userId
    $('#submitButton').click(function(){ 
	  userId=getCookie('userOpenId');
	  var comment=$("#TextArea1").val();
	  if(comment!=""){
         setCookie("comment",comment,365);
      }
	  var period=$(this).attr("data-period");
 	  if(userId!=""){	
		var curLength=$("#TextArea1").val().length;	
		if(curLength>=5&&curLength<=50){
     	  var text=$("#TextArea1").val();
		  var m_id=$("#m_id").val();
		  dataLayer.push({'event':'event','cat':'参赛作品','act':'评论成功','lbl':pageName});
          setTimeout('window.location="'+"addComment.php?text=" +encodeURIComponent(text)+"&&period="+period + "&&musicId=" + m_id+'"',500);
		}else{
          alert("字数不对");
        } 
	  }else{ window.location.href="login.php?pageName="+pageName;}	
	});
	
	
   $('#workDetailComment').click(function(){  
	if((i%2)!=0){
		$('#workDetailComment').removeClass("comment");
		$('#workDetailComment').addClass("workDetail");
		dataLayer.push({'event':'event','cat':'参赛作品','act':'作品详情','lbl':'作品信息'});
    	$('#workComment').css("display","block");
    	$('#commentSubmit').css("display","none"); 		
	}else{
		$('#workDetailComment').removeClass("workDetail");
		$('#workDetailComment').addClass("comment");
		dataLayer.push({'event':'event','cat':'参赛作品','act':'作品详情','lbl':'评论'});
		$('#workComment').css("display","none");
    	$('#commentSubmit').css("display","block");
		
	}
	i++;
  });	
 
})

function getCookie(Name) 
{ 
    var search = Name + "=" 
    if(document.cookie.length > 0) 
    { 
        offset = document.cookie.indexOf(search) 
        if(offset != -1) 
        { 
            offset += search.length 
            end = document.cookie.indexOf(";", offset) 
            if(end == -1) end = document.cookie.length 
            return unescape(document.cookie.substring(offset, end)) 
        } 
        else return "" 
    } 
}

function setCookie(c_name,value,expiredays)
{
var exdate=new Date()
exdate.setDate(exdate.getDate()+expiredays)
document.cookie=c_name+ "=" +escape(value)+
((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
} 

function checkCookie(){ 
  if(getCookie("comment")!=""){
     var commentContent=getCookie("comment");
	 $("#TextArea1").val(commentContent);
  }else{
	 $("#TextArea1").val("");
  }
} 
