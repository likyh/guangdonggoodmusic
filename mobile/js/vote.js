/**
 * Created by linyh on 14-3-18.
 */
$(document).ready(function(){
    // 直接投票、唯一码投票
    var $float=$("#float");
    var $layer;
    $float.hide();
    $(".iWantVoteActive,.voteActive").click(function(){
        alert("比赛已经结束");
        return false;
        musicId=$(this).attr("data-id");
		period=$(this).attr("data-period");
        $num=$(this).parent().parent().find(".vote_number,.voteNum");
        dataLayer.push({'event':'event','cat':'投票','act':'投票','lbl':musicId});
        $layer = $.layer({
            type: 1,
            title: false,
            closeBtn: false,
            border : [0, 0, '#666', true],
            bgcolor :null,
            area: ['544px','485px'],
            page: {
                dom : '#float'
            },
            success: function(){
                layer.shift('top',50);
                dataLayer.push({'event':'page','branch':'投票','section':'投票页面','pname':''});
            }
        });
		$float.find(".uniqueCode h4").html("输入编码为选手投票!<br>iPad好礼等你赢");
		$float.find(".uniqueCode h4").removeClass('messageColor');
		$float.find(".uniqueCode p").html("从促销装甜筒盖下获得编码。");
		$float.find(".qcode span").html("");
        return false;
    });

    $("#sureVote").click(function(){
        var uniqueCode=$("#uniqueCodeInput").val();
        dataLayer.push({'event':'event','cat':'投票','act':'确认投票','lbl':musicId});
        $float.find(".uniqueCode h4").html("提交中<br/>请稍等，投票信息提交中");
        $float.find(".uniqueCode p").html("");
        $float.find(".uniqueCode h4").removeClass('messageColor');
        $float.find(".qcode span").html("");
        var exitTime = new Date().getTime() + 800;
        if(uniqueCode!=""){
            $.ajax({
                type: "get",
                url: "voteAction.php?musicId="+musicId+"&uniqueCode="+uniqueCode+"&period="+period,
                dataType: "json",
                success: function (data) {
                if(data.message=='success'){
                    $float.find(".uniqueCode h4").html("投票成功!<br>继续为当前好歌投票！");
				    $float.find(".uniqueCode p").html("<a href='http://t.cn/8sl7Yk0' onclick=\"dataLayer.push({'event':'event','cat':'投票成功','act':'点击抽奖','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;\">现在就参与抽奖赢iPad> </a>");
					$float.find(".uniqueCode h4").removeClass('messageColor');
                    $num.text(Number($num.text())+5);
                    dataLayer.push({'event':'event','cat':'投票','act':'编码','lbl':musicId});
                }else{
                    $float.find(".uniqueCode h4").html(data.message);
					$float.find(".uniqueCode h4").addClass('messageColor');
					$float.find(".uniqueCode p").html("从促销装甜筒盖下获得编码，重新输入。");
					$float.find(".qcode span").html("");
                }
                $("#uniqueCodeInput").val("");
            }
            });
        }else{
            alert("请输入编码!");
        }
        return false;
    });
    $float.find("#close").click(function(){
        layer.close($layer);
        //$float.find(".message h4").text("提交中");
        //$float.find(".message p").html("正在提交您的请求，请稍等……");
        return false;
    });
});