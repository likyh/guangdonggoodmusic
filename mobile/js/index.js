/**
 * Created by linyh on 14-3-18.
 */

$(document).ready(function(){
    dataLayer.push({'event':'page','branch':'首页','section':'','pname':''});
    // 分享按钮
    $(".sharepopup").hide();
    $(".sharepopup").click(function(){
        var musicId=$(this).parent().parent().parent().attr("data-id");
        dataLayer.push({'event':'event','cat':'音乐分享','act':'分享媒体','lbl':musicId});
        $(".sharepopup").hide("fast");
    });
    $(".shareButton").click(function(){
        $(".sharepopup").hide("fast");
        $(this).parent().find(".sharepopup").show("fast");
        return false;
    });

    // 搜索按钮
    $("#projectSubmit").click(function(){
        var text=$("#projectInput").val();
        var $a=$(this);
        layer.tips('搜索中', $a, {guide: 1,time : 2});
        dataLayer.push({'event':'event','cat':'搜索','act':'搜索词','lbl':text});
        $.ajax({
            type: "get",
            url: "musicIdAction.php?text="+encodeURI(text),
            dataType: "json",
            success: function (data) {
                if(data.musicId!=0){
                    var $musicBox=$.layer({
                        type : 2,
                        title : text,
                        iframe : {src : 'http://l.5sing.com/player.swf?autostart=true&songtype=yc&songid='+data.musicId},
                        area : ['250px' , '68px'],
                        border : [20, 0.5, '#666', true],
                        offset : ['200px','']
                    });
                }else{
                    layer.tips('无结果', $a, {guide: 1,time : 2});
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                layer.tips('无结果', $a, {guide: 1,time : 2});
            }
        });
        return false;
    });

    // 播放按钮
    if(!navigator.userAgent.match(/.*Mobile.*/)){
        $(".musicPlay").attr("href","#");
        $(".musicPlay").click(function(){
            var $tr= $(this).parent().parent();
            var $title=$tr.find(".title").text();
            var musicId=$tr.attr("data-id");
            dataLayer.push({'event':'event','cat':'歌曲','act':'播放','lbl':$title});
            var $a=$.layer({
                type : 2,
                title : $title,
                iframe : {src : 'http://l.5sing.com/player.swf?autostart=true&songtype=yc&songid='+musicId},
                area : ['250px' , '68px'],
                border : [20, 0.5, '#666', true],
                offset : ['200px',''],
                close : function(index){
                    dataLayer.push({'event':'event','cat':'歌曲','act':'完成','lbl':$title});
                }
            });
            return false;
        });
    }


    // 直接投票、唯一码投票
    var $float=$("#float");
    var $layer;
    $float.hide();
    $(".voteButton").click(function(){
        $layer = $.layer({
            type: 1,
            title: false,
            closeBtn: false,
            border : [0, 0, '#666', true],
            bgcolor :null,
            area: ['783px','386px'],
            page: {
                dom : '#float'
            },
            success: function(){
                layer.shift('top',500);
            }
        });
        var musicId=$(this).parent().parent().parent().attr("data-id");
        var $num=$(this).parent().find(".num");
        dataLayer.push({'event':'event','cat':'投票','act':'投票','lbl':musicId});
        $.ajax({
            type: "get",
            url: "../voteAction.php?musicId="+musicId,
            dataType: "json",
            success: function (data) {
                if(data.message=='success'){
                    $float.find(".message h4").text("投票成功");
                    $float.find(".message p").html("感谢您对好歌歌手的鼎力支持！<br/>您今天的投票已经用完了");
                    $num.text(Number($num.text())+1);
                }else{
                    $float.find(".message h4").text("投票失败");
                    $float.find(".message p").html(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $float.find(".message h4").text("投票失败");
                $float.find(".message p").text(errorThrown);
            }
        });
        $("#sureVote").click(function(){
            dataLayer.push({'event':'event','cat':'投票','act':'编码','lbl':''});
            $float.find(".message h4").text("提交中");
            $float.find(".message p").html("正在提交您的请求，请稍等……");
            var uniqueCode=$("#uniqueCodeInput").val();
            $.ajax({
                type: "get",
                url: "../voteAction.php?musicId="+musicId+"&uniqueCode="+uniqueCode,
                dataType: "json",
                success: function (data) {
                    if(data.message=='success'){
                        $float.find(".message h4").text("投票成功");
                        $float.find(".message p").html("您支持的选手获得了您的5票");
                        $num.text(Number($num.text())+5);
                    }else{
                        $float.find(".message h4").text("投票失败");
                        $float.find(".message p").text(data.message);
                    }
                }
            });
            return false;
        });
        return false;
    });
    $float.find("#close").click(function(){
        layer.close($layer);
        $float.find(".message h4").text("提交中");
        $float.find(".message p").html("正在提交您的请求，请稍等……");
        return false;
    });
});