<?php
include "check.php";
?><!DOCTYPE html >
<html>
<head>
   <meta charset="utf-8"  name="keywords" content="广东好歌比赛进程，广东好歌评选机制，广东好歌报名方式，广东好歌报名条件">
	<meta charset="utf-8" name="description" content="广东好歌选秀大赛官方网站为您提供广东好歌比赛进程，广东好歌参赛规则，报名条件及评分机制，让您更清楚的了解广东好歌。" >
	<title>【活动说明】比赛进程_参赛规则_报名条件_评分机制-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
    <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/activity.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="shuoming.php";
		pn="activity";
        if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
     <script type="text/javascript" src="../script/shareLink.js"></script>
     <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
<?php include_once "header.php"; ?>
<div id="activity">
 <div class="navigationButton1">
            <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
        </div>
  <article>
        <!--活动说明-->
  <div id="actIll">
  <img src="images/line1.png"/>
    <h4>和你爱的广东有个粤会</h4>
    <p>
      每一个出生、成长、生活、或喜欢广东的人，对她有着非同一般的情感。一千个人有一千种对广东的认识，有没有一首歌代表你<br>
      心目中的广东？承载纯正广东文化的广东知名雪糕品牌五羊，携全球知名音乐公司索尼音乐娱乐、国内顶尖数字音乐平台酷狗音乐，<br>
      为每个热爱广东文化的你联手打造广东原创歌曲网络比赛，以新颖好玩的方式，用音乐和你爱的广东来个粤会吧！

      
      </p>
    <p> 主办方：五羊雪糕； 承办方：索尼音乐娱乐，酷狗音乐。</p>
    <a href="http://gdhg.5sing.com/gdhg/index.html "id="joinNow"></a>
    <h4>粤唱进程:</h4>
    <br>
    <img src="images/shuomingpic.jpg"/>
    <p>*比赛结果将在活动网站的《赛事报道》中予以发布</p>
    <div class="whitetxt">
    
      <h4>粤唱规则：</h4>
      <p>只要你热爱广东文化，不限年龄、性别、居住所在地，无论是个人、组合、乐队等多种形式，均可以参加，<br>
        唱出心目中的广东。</p>
      <p><span>歌曲主旨：</span>以弘扬广东文化为主旨，内容需包含与广东文化相关的元素</p>
      <p><span>作品要求：</span>参赛作品需为原创。唱词需全部或部分包含粤语元素。</p>
      <p><span>作品长度：</span>参赛者需上传完整的原创作品或至少该作品的一段完整主歌和副歌。</p>
      <p><span>上传格式：</span>海选阶段只需上传MP3格式的作品，第二轮及第三轮比赛需上传MP4格式的视频。</p>
     
      <h4><span>评分机制：</span></h4>
      <p>由网络观众投票和专业评审团（陈柏宇，东山少爷，三少，陈浩然）打分两部分综合评选</p>
      <p>【网友投票数*平均评委星星数】= 作品总分</p>
      <p>　　每一位评委有五颗星，以半颗星为间隔，划分为10个标准（0.5颗星，1颗星，1.5颗星 … 5颗星）</p>
      <p>　　例如：【6,400票数*3.5颗星】 = 22,400总分</p>
      <h4>网友投票:</h4>
      <p>使用编码（从五羊甜筒盖下）可获得五票。</p>
      <p>五票只能投给同一名选手</p>
      <p>每天允许使用多个编码</p>
        
      <h4>专业评审团评分标准</h4>
      <p>第一轮：上传自创音频，词曲分各占50%</p>
      <p>第二轮：上传自制视频，词曲分各占45%，视频质量10%</p>
      <p>第三轮：上传自制视频，词曲分各占45%，视频质量10%</p>
      <p>决  赛：依据选手现场表演，由专业评审团及粉丝酷评团打分</p>
    </div>
  </div>
  </article>
    </div>
</div>
</div>
<!--END #container-->
</body>
</html>
