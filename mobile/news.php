<?php include "check.php"; 
   include_once "../lib/data.class.php";
   $data=new Data();
   $info=$data->getSixNewsList();
?><!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8">
    <meta  name="keywords" content="赛事报道">
	<meta name="description" content="广东好歌选秀大赛官方网站为您提供广东好歌赛事全程报道，让您第一时间看到广东好歌赛事的最近进展和赛事热点。" >
	<title>【赛事报道】选秀赛事报道,大赛赛事报道-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
    <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="news.php";
		pn="news";
        if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
      <script type="text/javascript" src="js/highlight.js"></script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
  
</head>

<body>

<!--BEGIN #container-->
<div id="container">
    <?php include_once "header.php"; ?>
    <div id="content" class="news">
     
        <div class="newsItem">
        <ul>
         <?php foreach($info as $result) {?>
           <li>
            <a href="newsDetail.php?newsId=<?php echo $result['id'];?>" onClick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'图片'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;
"><img src="http://gdhg.kugou.com/<?php echo $result['picurl']; ?>" alt=""></a>

           <a href="newsDetail.php?newsId=<?php echo $result['id'];?>" onClick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'标题'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;
"> <h3>
   <?php $title=$result['title']; if(mb_strlen($title,"utf8")>18){echo mb_substr(strip_tags($title),0,18,"utf8")."...";}else{echo $title;}?>
</h3> </a>
            <h4>时间:<span id="time"><?php echo $result['create_time']; ?></span></h4>
            <p>
            <?php echo mb_substr(strip_tags($result['content']),0,45,'utf8');?>
            </p>
            <a href="newsDetail.php?newsId=<?php echo $result['id'];?>" onClick="dataLayer.push({'event':'event','cat':'赛事报道','act':'赛事报道列表','lbl':'查看更多'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;
"><h5>查看详情&gt;</h5></a>
          
            </li>
            <?php }?>
         </ul>
        </div>  
    </div>
</div>

</body>
</html>
