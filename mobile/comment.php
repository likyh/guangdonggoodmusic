<?php include "check.php";
 include_once "../lib/game_work.class.php";
 include_once "../lib/data.class.php";
 include_once "../lib/imageURL.php";
 $imgUrl=new ImageUrl;
 if(isset($_GET['musicId'])){
   $musicId=$_GET['musicId'];
   if(isset($_GET['period'])){
   $curperiod=$_GET['period'];
   }else{$curperiod=1;}
} 
 $data=new Data();
 $comment_result=$data->getComment($musicId);
 $period=$data->getPeriod();
 $commentArray=array();
 if($comment_result['comment']!=null ){
  $commentArray=$comment_result['comment'];
 }else{
  
 }
 $work_info=new GameWork();
 //$result=$work_info->getMusicMessage($musicId);
 $resultset=$data->getOneWorks($musicId,$curperiod);
 $result=$resultset['music'];

 $next=$resultset['nextId'];
 $prev=$resultset['preId'];

 $rankNumber=$data->voteRank($musicId,$curperiod);
 $voterank=$rankNumber['rank'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta  name="keywords" content="<?php echo $result['title'];?>">
	<meta name="description" content="<?php echo mb_substr(htmlentities(strip_tags($result['introduction']),ENT_QUOTES,'UTF-8'),0,200,'utf8');?>" >
	<title>参赛作品详情标题-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
    <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/comment.js"></script>
    <script type="text/javascript" src="js/layer.min.js"></script>
    <script type="text/javascript" src="js/vote.js"></script>
     <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="comment.php";
		pn="music";
		params=window.location.search;
		pageName+=params;
		if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
    <script type="text/javascript" src="js/highlight.js"></script>
</head>
<body>
<!--BEGIN #container-->
<div id="container">
<?php include_once "header.php"; ?>
<!--BEGIN #topmain-->
<div id="topMain">
  <div id="votedRanking">
    <p>票选排名:<span class="voteRankNumber"><?php echo $voterank;?></span></p>
    
    <?php $title=$result['title'];
		if(mb_strlen($title,"utf8")>10){
			echo "<h1 class='voteRankTitle'>".$title."</h1>";}
		else{echo "<h1 class='voteRankTitle'>".$title."</h1>";}?>
        
    <div class="preAndNext"> 
    	<?php if($prev!=null){ ?><a href="comment.php?musicId=<?php echo $prev; ?>&period=<?php echo $curperiod;?>" id="preSong"></a> <?php } ?>
        <?php if($next!=null){?><a href="comment.php?musicId=<?php echo $next; ?>&period=<?php echo $curperiod;?>" id="nextSong"></a> <?php } ?>
    </div>
  </div>
  <?php if($curperiod==$period){ ?> 
  <div id="iWantVote"  class="iWantVoteActive" data-period="<?php echo $period; ?>" data-id="<?php echo $result['id']?>">
      <span class="vote_number voteNum"><?php echo $result['vote']?></span>     
            <a href="#" id="voteButton" ></a>
  </div>
   <?php }else{ ?>
  <div id="iWantVote" data-period="<?php echo $period; ?>" data-id="<?php echo $result['id']?>">
      <span class="vote_number voteNum"><?php echo $result['vote']?></span>     
            <a href="#"></a>
  </div>
  <?php } ?> 
  <audio src="<?php echo $result['music_url'] ?>" controls preload autoplay>抱歉，您的浏览器不支持该格式的音乐</audio>
</div>
<!--End #topMain-->
<!--Begin #bottomMain-->
<div id="bottomMain">
  <div class="workCommentGuide">
    <div id="workDetailComment"></div>
  </div>
  <div class="workComment" id="workComment">
    <div class="workDetail"> <img src="images/triange1.png"/>
      <h3 class="workdetail">创作灵感:</h3>
       <div id="share">分享作品到:<?php include "part/shareLink.php";?></div>
      <p><?php echo $result['inspiration'];?></p>
      <h3>选手信息:</h3>
      <div class="singer"> <img src="<?php $pic_url=$result['photo']; $picurl=$imgUrl->changeUrl($pic_url,"_193_193");echo $picurl; ?>"/>
        <h4>选手姓名:<span id="playername"><?php echo $result['singer'];?></span></h4>
        <p>选手介绍:<span><?php echo $result['profile'];?></span></p>
        <h4>                                         </h4>
        
      </div>
      
    </div>
  </div>
  <div class="commentSubmit" id="commentSubmit"> <img src="images/triange.png"/>
    <div id="textandcomment"> <span id="textCount"></span>
      <textarea name="comments" rows="6" cols="50" id="TextArea1"></textarea>
      <input type="submit"  value="评价" id="submitButton" data-period=<?php echo $curperiod;?> />
      <input type="hidden"  value="<?php echo $result['id'] ;?>" id="m_id"/>
    </div>
    <div class="commentShowall">
      <h3>全部评论</h3>
      <?php if($commentArray!=null){foreach($commentArray as $res){?>
      <div id="singleComment">
        <p><span class="name">	 
         <?php 
		   $username=$res['username'];
		   echo "<script type='text/javascript'> var name=unescape('".$username."');document.write(name);</script>";
		 ?>
         </span>发布于<span class="time"><?php echo $res['timestamp'];?></span></p>
        <div><?php echo mb_substr(htmlentities(strip_tags($res['content']),ENT_QUOTES,'UTF-8'),0,50,'utf8');?></div>
      </div>
      <?php } }?>
    </div>
  </div>
  <!--End #bottomMain-->
    <div id="float" class="show">
        <a id="close" href="#"></a>
       <!-- <div class="message">
            <h4>提交中</h4>
            <p>正在提交您的请求，请稍等……</p>
        </div>-->
        <div class="uniqueCode">
            <h4>您也可以直接输入产品编码为选手投票！</h4>
        	<p>购买五羊甜筒,从甜筒盖下获取编码！</p>
            <input type="text"  id="uniqueCodeInput"/>
            <a href="#" data-period="<?php echo $period; ?>" id="sureVote" >确认投票</a>
            <div class="qcode"><span></span></div>
        </div>
    </div>
</div>
</div>
</body>
</html>
