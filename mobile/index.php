<?php include "check.php";
  include_once "../lib/game_work.class.php";
  include_once "../lib/data.class.php";
  include_once "../lib/imageURL.php";
  $data=new Data();
  $imgUrl=new ImageUrl;
  $maxPeriod=$data->getPeriod();
  $period=$maxPeriod; 
  $gw=new GameWork();
  $resultset=$gw->getEntriesByPeriod($period);
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8" />
     <meta  name="keywords" content="参赛作品，参赛作品大全，试听参赛歌曲">
	<meta name="description" content="广东好歌选秀大赛官方网站参赛作品频道为您提供最全的广东好歌参赛作品，试听广东好歌参赛作品，免费试听广东好歌参赛作品。" >
	<title>【参赛作品】参赛作品大全，试听参赛歌曲-广东好歌选秀大赛官方网站</title>
     <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
     <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/gameWorkSearch.js"></script>
    <script type="text/javascript" src="js/layer.min.js"></script>
    <script type="text/javascript" src="js/vote.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="index.php";
		pn="index";
		if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
    <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
 <!--BEGIN #container-->
<div id="container">
    <?php include_once "header.php"; ?>
    <div id="main" class="gameWork">
    <a href="http://wuyang.wc.m0.hk/?utm_source=owned&utm_medium=mobile_kv&utm_campaign=2014q1_5rams_mobile"
    	onclick="dataLayer.push({'event':'event','cat':'kv','act':pageName,'lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">
    	<div id="game_progress"></div>
    </a>
    
   <!-- <div id="search_work">
      <input name="" type="text" id="projectInput" placeholder="作品名/选手名"/>
      <a id="projectSubmit" href="#" data-period="<?php/* echo $period;*/?>" ></a>   
    </div>-->
    
   <!-- <p class="tips"><span class="tip">温馨提示：</span>如果你想查看往期权全部作品请到PC版活动网站查看</p>-->
    
    <div id="game_works">
     <?php foreach($resultset as $result){?>
     
      <input type="hidden" value="<?php echo $result['id']?> " id="m_id"/>
      <div id="game_work">
       <a href="comment.php?musicId=<?php echo $result['id']?>&period=<?php echo $period;?>"
          onclick="dataLayer.push({'event':'event','cat':'活动首页','act':'作品列表','lbl':'图片'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">
           <img src="<?php $pic_url=$result['pic_url']; $picurl=$imgUrl->changeUrl($pic_url,"_238_119");echo $picurl; ?>"/></a>
      <div class="game_work_title">
        <p class="song_name">
            <a href="comment.php?musicId=<?php echo $result['id']?>&period=<?php echo $period;?>"
               onclick="dataLayer.push({'event':'event','cat':'活动首页','act':'作品列表','lbl':'文字'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">
                <?php $title=$result['title']; if(mb_strlen($title,"utf8")>8){echo mb_substr(strip_tags($title),0,8,"utf8")."...";}else{echo $title;}?>
            </a>
        </p>
        <p class="singername"><?php echo $result['singer']?></p>
      </div>
     </div>
     <?php } ?>     
    </div>
    </div>
    <div id="float" class="show">
        <a id="close" href="#"></a>
       <!-- <div class="message">
            <h4>提交中</h4>
            <p>正在提交您的请求，请稍等……</p>
        </div>-->
        <div class="uniqueCode">
           <h4>您也可以直接输入产品编码为选手投票！</h4>
           <p>购买五羊甜筒,从甜筒盖下获取编码！</p>
            <input type="text" id="uniqueCodeInput"/>
            <a href="#"  data-period="<?php echo $period; ?>"  id="sureVote">确认投票</a>
            <div class="qcode"><span></span></div>
        </div>
    </div>
</div>
    
</body>
</html>
