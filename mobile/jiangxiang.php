<?php
include "check.php";
?><html>
<head>
    <meta charset="utf-8" name="keywords" content="广东好歌奖项">
	<title>【奖项设置】-广东好歌选秀大赛官方网站</title>
    <!--<meta name="viewport" content="width=640px, user-scalable=no"/>-->
    <meta name="viewport" content="target-densitydpi=320,width=640,user-scalable=no,maximum-scale=1.5">
     <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/activity.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="js/reset.js"></script>
    <script type="text/javascript" src="../script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="jiangxiang.php";
		pn="activity";
        if(getCookie('userOpenId')!=""){
	 		userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'mobile','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="../script/shareLink.js"></script>
     <script type="text/javascript" src="js/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
<?php include_once "header.php"; ?>
   <div id="activity">
    <div class="navigationButton3">
       <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
    </div>
  <article>
  <!--奖品设置-->
  <img src="images/line3.png"/>
  <div id="prizeSet">
    <h4>网友抽奖</h4>
    <p>1.在活动期间，购买五羊促销装甜筒，从甜筒盖下获得编码。<br>
      2.关注五羊雪糕官方微信账号，点击"码上有奖"按钮，输入五羊促销装甜筒盖下的编码，即可参与抽奖。<br>
      3.一个编码一次抽奖机会。 </p>
    <img src="images/prize.png"/> 
    <h6>*苹果公司非联合活动商亦非本次活动赞助商<br>
    **索尼音乐提供旗下艺人在广东省内举办的演唱会、歌友会、见面会。试用期至2015/5/21</h6>
     <br>
    <h4>参赛者</h4>
    <h4>冠军将获得：</h4>
    <p>1. 由索尼音乐娱乐主理的专业录音制作（一首单曲）
</p>
    <p>2. 由索尼音乐娱乐主理的专业制作MV（一首单曲）
</p>
    <p>3. 索尼音乐娱乐对本单曲的宣传及数字发行
</p>
<h4>总决赛选手将获得：</h4>
<p>与明星评审、音乐评审最后直播演出的表演机会</p>
</div>
  </article>
    </div>

</div>
<!--END #container-->
</body>
</html>
