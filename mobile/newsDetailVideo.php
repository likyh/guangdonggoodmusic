<?php include "check.php";
   include_once "../lib/data.class.php";
   if(isset($_GET['newsId'])&&!empty($_GET['newsId'])){
     $newsId=$_GET['newsId'];
   }else{
     $newsId=1;
   }
   $data=new Data();
   $info=$data->getNewsListDetailed($newsId);
   $otherInfo=$data->getOtherDetail($newsId);
?><!DOCTYPE html >
<html >
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="images/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="images/css/style.css"/>
    <title>赛前活动新闻详情</title>
</head>

<body>
 <div id="container">
    <?php include_once "header.php"; ?>
   <div id="content" class="newsDetail">
        <div id="article">
                <div class="newsTitle">  
                    <h1><?php echo $info['title'];?></h1>
                    <h4>时间:<span id="time"><?php echo $info['create_time'];?></span><a href="news.php">返回列表&gt;&gt;</a></h4>  
                </div>
                <div class="mainDetail">
                <video width="320" height="240" controls="controls"> 
      <source src="http://vjs.zencdn.net/v/oceans.mp4" type="video/mp4"> 
      <object data="" width="320" height="240">     
        <embed width="320" height="240" src="http://vjs.zencdn.net/v/oceans.mp4"> 
      </object> 
    </video>
                
                <p><?php echo $info['content'];?></p>    
                </div> 
        </div>
        
        <div id="share">分享到：
        <a class="bds_tsina" title="分享到新浪微博" href="#"><img src="images/sina.png"/></a> 
        <a class="bds_renren" title="分享到人人网" href="#"><img src="images/renren.png"/></a> 
        <a class="bds_douban" title="分享到豆瓣网" href="#"><img src="images/douban.png"/></a> 
        <a class="bds_tengxun" title="分享到腾讯微博" href="#"><img src="images/tengxun.png"/></a>
        </div>
        
        <div id="moreNews">
          <h2>更多新闻</h2>
          <div class="newsItem">
        <ul>
          <?php foreach($otherInfo as $result){?>
           <li>
            <a href="newsDetail.php?newsId=<?php echo $result['id']; ?>">
            <img src="<?php echo $result['picurl']; ?>" alt="标题">
            <h3><?php echo $result['title']; ?></h3>
            <h4>时间:<span id="time"><?php echo $result['create_time']; ?></span></h4>
            <p><?php echo $result['content']; ?></p>
            <h5>查看详情&gt;</h5>
            </a>
            </li>
          <?php  }?>  
         </ul>
        </div>  
            
        </div>
    </div>
</div>
</body>
</html>

