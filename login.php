<?php
include_once('lib/kugouConfig.php'); 
$pageName=$_GET["pageName"];

if(isset($_GET['period'])&&!empty($_GET['period'])){
  $period=$_GET["period"];
  $params="&period=".$period;
  $pageName=$pageName.$params;
}
$_SESSION['pageName']=$pageName;
setcookie("pageName",$_SESSION['pageName'], time()+3600*24);

$client_id = KUGOU_AKEY;
$redirect_uri = urlencode(LOGIN_CALLBACK);
$authorize_url = KUGOU_AUTHORIZE_API.'&client_id='.$client_id.'&redirect_uri='.$redirect_uri;
//手机端调用 $authorize_url .= '&wap=1';
header("Location:".$authorize_url);