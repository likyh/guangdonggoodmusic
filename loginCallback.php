<?php
/**
 * 酷狗登录网站接入demo
 * @author josephzeng
 * 2014-04-01
 **/
session_start();
include_once('lib/kugouConfig.php');
include_once('lib/common.php');
$code = isset($_GET['code']) ? trim($_GET['code']) : '';
if($code){
	try{
		$json = get_data(KUGOU_TOKEN_API.'&client_id='.KUGOU_AKEY.'&client_secret='.KUGOU_SKEY.'&code='.$code);
		if($json){
			$arr = json_decode($json, true);
			if($arr['status'] == 1){
				$access_token = $arr['access_token'];				
				if($access_token){
					$json = '';
					$json = get_data(KUGOU_OPENID_API.'&access_token='.$access_token);
					if($json){
						$arr = array();
						$arr = json_decode($json, true);
						if($arr['status'] == 1){
							$openid = '';
							$openid = $arr['openid'];
							if($openid){
								$json = '';
								$json = get_data(KUGOU_USERINFO_API.'&access_token='.$access_token.'&oauth_consumer_key='.KUGOU_SKEY.'&openid='.$openid);
								$arr = array();
								$arr = json_decode($json, true);
								if($arr['status'] == 1){
                                    //open id
                                    $_SESSION['userOpenId'] = $arr['openid'];
									setcookie("userOpenId",$_SESSION['userOpenId']);
                                    //昵称
                                    $_SESSION['userInfo']['nickname'] = $arr['nickname'];
                                    
									
									//用户名
                                    $_SESSION['userInfo']['username'] = $arr['username'];
                                    //头像
                                    $_SESSION['userInfo']['pic'] = $arr['pic'];
									
									//$pageName=$_COOKIE['pageName'];	
									//echo $pageName;	
									 //header("Location:".$pageName);
									 
									 // echo $pageName;
									// $s="Location:".$pageName;
									 /*echo "<script>alert('到达返回页面 $s');window.location.href='$pageName';</script>";
									echo "<script>alert('到达返回页面 $pageName');window.open('$pageName','_self');</script>";*/
									if(isset($_COOKIE['pageName'])&&!empty($_COOKIE['pageName'])){
										$pageName=$_COOKIE['pageName'];	
										header("Location:".$pageName);
									}else{
										header("Location:index.php");
									} 
                                    exit();
								}else{
									/* view $arr['msg'] */
								}
							}else{
								/* view openid null */
							}
						}else{
							/* view $arr['msg'] */
						}
					}else{
						/* view get data fail */
					}
				}else{
					/* view access_token null */
				}
			}else{
				/* view $arr['msg'] */
			}
		}else{
			/* view get data fail */
		}
	}catch(Exception $e){
		/* view $e->getMessage() */
	}
}else{
	/* redirct fail */
}
echo "<meta charset='utf-8'>您的网速过慢或者其他网络原因，请刷新重试，或者联系网络管理员";
