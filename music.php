<?php
include "check.php";
 include_once('lib/data.class.php');
 include_once('lib/news.class.php');
 $gr=new GameReport();
 $data=new Data();
 $period="";
 $maxPeriod=$data->getPeriod();
 if(isset($_GET['period'])&&$_GET['period']<=5&&$_GET['period']>=1){
    $period=$_GET['period'];
 }else{$period=$maxPeriod;}

$period=(int)$period;
 $sql="select count(*) from music where period={$period}";
 $rows=$gr->getRows($sql);
 if($rows>0){
   $pagesize=9;
   $pages=$gr->getPages($rows,$pagesize);

   $currentpage=isset($_GET['page'])?(int)$_GET['page']:1;
   if($currentpage<1){
     $currentpage=1;
   }else if($currentpage>$pages){
	 $currentpage=$pages;
   }
   $prevPage=$currentpage-1>0?$currentpage-1:1;
   $nextPage=$currentpage+1<=$pages?$currentpage+1:$pages;
   $pageresultset=array();
   if(isset($_GET['bytime'])&&$_GET['bytime']=="1"){
     $bytime=1;
     $sortBy="time";
     $resultset=$data->pagingBycreate_time($currentpage,$pagesize,$period);
   }else{
     $bytime=0;
     $sortBy="vote";
    $resultset=$data->pagingByVote($currentpage,$pagesize,$period);
   }
 }else{
   $currentpage=1;
   //6/13号ds添加
   if(isset($_GET['bytime'])&&$_GET['bytime']=="1"){
     $bytime=1;
     $sortBy="time";
   }else{
     $bytime=0;
     $sortBy="vote";
   }
   //
 } 
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta  name="keywords" content="参赛作品，参赛作品大全，试听参赛歌曲">
	<meta name="description" content="广东好歌选秀大赛官方网站参赛作品频道为您提供最全的广东好歌参赛作品，试听广东好歌参赛作品，免费试听广东好歌参赛作品。" >
	<title>【参赛作品】参赛作品大全，试听参赛歌曲-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/music.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="music.php";
		pn="works";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/layer.min.js"></script>
    <script type="text/javascript" src="script/vote.js"></script>
    <script type="text/javascript" src="script/music_search.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
   
</head>

<body>
<!--BEGIN #container-->
<div id="container">
  <div id="header">
    <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
	<div id="personalCenter"><?php include 'part/personalCenter.php'; ?></div>
    <div id="title">
      <div class="button"><?php include_once "part/navigation.php";?></div>
    </div>
  </div>  
    <div id="content" class="music">
      <div id="left">
        <div id="period">
            <ul>
                <li class="period1"><a href="music.php?period=1" <?php if($maxPeriod<1){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==1?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'海选'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
                <li class="period2"><a href="music.php?period=2" <?php if($maxPeriod<2){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==2?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'五十进二十五强'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
                <li class="period3"><a href="music.php?period=3" <?php if($maxPeriod<3){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==3?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'二十五进十强'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
                    <li class="period4"><a href="music.php?period=4" <?php if($maxPeriod<4){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==4?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'复活赛'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
                    <li class="period5"><a href="music.php?period=5" <?php if($maxPeriod<5){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==5?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'十二强进决赛'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
                <li class="period6"><a href="music.php?period=6" <?php if($maxPeriod<6){echo "onclick='alert(\"该阶段尚未开始，敬请期待！\");return false'";}?> class="<?php echo $period==6?"periodActive":""; ?>"
                    onclick="dataLayer.push({'event':'event','cat':'参赛作品','act':'二级导航','lbl':'总决赛'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a></li>
            </ul>
        </div>
        <div id="uploading"><a href="http://www.5sing.com/login.aspx?Url=http://member.5sing.com/Writing/VideoAdd.aspx"><img src="style/images/uploadbtn.png" id="upimg"/></a></div>
      </div>
        <div id="main">
            <div id="time">活动时间:
             <?php  switch($period){
                            case 1:$periodTemp="3月19日-4月21日";break;
                            case 2:$periodTemp="4月21日-5月19日";break;
                            case 3:$periodTemp="5月19日-6月18日";break;
                            case 4:$periodTemp="6月19日-6月21日";break;
                            case 5:$periodTemp="6月21日-7月19日";break;
                            case 6:$periodTemp="7月23日";break;
                        } echo $periodTemp;?>
            </div>
            <div class="filter">
                <div id="project">
                    <label for="projectInput">参赛作品</label>
                    <div id="sourceForm">
                        <input type="text" id="projectInput" name="project" placeholder="请输入作品名\参赛选手名" />
                        <a id="projectSubmit" href="#" data-period="<?php echo $period; ?>">搜索</a>
                        <!-- <input type="hidden" value="<?php /*?><?php echo $period?><?php */?>" id="hiddenperiod" />-->
                    </div>
                </div>

                 <div id="scanDiv">
                    <span>浏览方式：</span>
                    <a href="music.php?page=<?php echo $currentpage;?>&amp;period=<?php echo $period?>&amp;bytime=<?php echo 1-$bytime;?>">
                        <div class="<?php echo $sortBy; ?>"></div></a>
                </div>
            </div>
           
            <?php if($rows>0){ ?>
            <div id="works">
                <?php foreach($resultset as $result){ ?>
                <div class="musicItem">
                   <div class="musicImg">
                    <a href="worksdetail.php?musicId=<?php echo $result['id']?>&period=<?php echo $period; ?>"><img src="<?php echo $result['pic_url']; ?>"/><img src="style/images/videoplaybtn.png" id="playLogo"/></a>
                    </div>
                    
                   <h4><a href="worksdetail.php?musicId=<?php echo $result['id'];?>&period=<?php echo $period; ?>">
                  <?php $title=$result['title'];
				            if(mb_strlen($title,"utf8")>10){				
				 				echo mb_substr(strip_tags($title),0,10,"utf8")."...";
							}else{
				 				echo $title;}?></a></h4>
                  
                   <span class="singer"><?php echo $result['singer']; ?></span>
                    <?php if($period==6){ ?>
                    <?php }elseif($maxPeriod==$period){ ?>
                   <div class="voteDiv voteDivClick" data-period="<?php echo $period; ?>" data-id="<?php echo $result['id']; ?>">		                
                    <span class="num"><?php echo $result['vote']; ?></span>
                    <a class="voteButton" href="#" >投票</a> 
                   </div>
                   <?php }else{?>
                   <div class="voteDiv">		                
                    <span class="num"><?php echo $result['vote']; ?></span>
                    	<a class="nagitiveVoteButton" href="#">票数</a> 
                   </div>
                   <?php } ?> 
                   
                   
                </div>
                <?php } ?>
            </div>
            <?php }?>
            <?php if($rows>0){ ?>
            <div id="page">
                <a href="music.php?page=<?php echo $prevPage; ?>&amp;bytime=<?php echo $bytime;?>&period=<?php echo $period; ?>" >&lt;</a>
                <?php for($i=$currentpage-3>0?$currentpage-3:1;$i<=($currentpage+3<$pages?$currentpage+3:$pages);$i++){?>
                <a href="music.php?page=<?php echo $i; ?>&amp;bytime=<?php echo $bytime;?>&period=<?php echo $period; ?>" class="<?php if($currentpage==$i) echo "page_active"; ?>"><?php echo $i; ?></a>
                <?php } ?>
                <a href="music.php?page=<?php echo $nextPage; ?>&amp;bytime=<?php echo $bytime;?>">&gt;</a>
            </div>
            <?php }?>
        </div>        
    </div>
    <?php include_once "part/voteFloat.php"; ?>
    <?php include_once "part/footer.php"; ?>
</div>

</body>
</html>
