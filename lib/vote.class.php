<?php
include_once 'SqlDB.class.php';
include_once 'ip.php';
class Vote {
    /**
     * @var SqlDB
     */
    protected $db;
    function __construct(){
        $this->db= SqlDB::init();
    }

    /**
     * 投票
     * @param $openid
     * @param $musicId
     * @return string
     */
    public function vote($openid,$musicId,$period,$terminal){
        $db=$this->db;
        $openid=$db->quote($openid);
        $musicId=(int)$musicId;
        $terminal=(int)$terminal;
        $existSql="select 1 from vote_history where `open_id`={$openid} and `date`=DAYOFYEAR(curdate()) and `music_id`={$musicId}";
        if($db->getExist($existSql)){
            $message="使用五羊雪糕编码继续为选手投票！";
        }else{
            $ip=getIP();
            $insertSql="INSERT INTO `vote_history` (`open_id`,`date`,`music_id`,`ip`,`terminal`)VALUES
({$openid},DAYOFYEAR(curdate()),{$musicId},'{$ip}',{$terminal});";
            $updateSql="UPDATE `music` SET `vote` = `vote`+1 WHERE `id`={$musicId} and `period`={$period};";
            try{
                $insertResult=$db->sqlExec($insertSql);
                $updateResult=$db->sqlExec($updateSql);
                if($insertResult>0 &&$updateResult>0){
                    $message="success";
                }else{
                    $message="投票失败";
                }
            }catch (PDOException $e){
//                    echo $e;
                $message="投票失败";
            }
        }
        return $message;
    }

    /**
     * 投票
     * @param $openid
     * @param $musicId
     * @return string
     */
    public function voteUniqueCode($uniqueCode,$musicId,$period,$terminal){
        $db=$this->db;
        $uniqueCodeTemp=$uniqueCode;
        $uniqueCode=$db->quote($uniqueCode);
        $existSql="select 1 from unique_code where `unique_code`={$uniqueCode} and `music_id`=0;";
        if($db->getExist($existSql)){
            $ip=getIP();
            /*$openid=$db->quote($openid);*/
            $musicId=(int)$musicId;
            $terminal=(int)$terminal;
            $uniqueCodeSql="UPDATE `unique_code` SET `music_id`={$musicId} WHERE `unique_code`={$uniqueCode};";
            $musicSql="UPDATE `music` SET `vote` = `vote`+5 WHERE `id`={$musicId} and `period`={$period};";
            $insertVoteHis="INSERT INTO `vote_history` (`date`,`music_id`,`ip`,`unique_code`,`terminal`)VALUES
(DAYOFYEAR(curdate()),{$musicId},'{$ip}',{$uniqueCode},{$terminal});";
//            var_dump($insertVoteHis);
//            var_dump($uniqueCodeSql);
//            var_dump($musicSql);
            try{
                $uniqueCodeResult=$db->sqlExec($uniqueCodeSql);
                $musicResult=$db->sqlExec($musicSql);
                $insertVoteHisResult=$db->sqlExec($insertVoteHis);
                if($uniqueCodeResult>0&& $musicResult>0&& $insertVoteHisResult>0){
                    $message="success";
                }else{
                    $message="投票失败";
                }
            }catch (PDOException $e){
//                    echo $e;
                $message="投票失败";
            }
        }else{
            $message="投票失败<br>编码不存在或已被使用";
        }
        return $message;
    }
} 