<?php
/**
 * 获取客户端的ip，使用一个整数表示，而不是四个数
 */
function getIP(){
	if(!empty($_SERVER["HTTP_CLIENT_IP"])){
		$cip = ip2long($_SERVER["HTTP_CLIENT_IP"]);
	}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$cip = ip2long($_SERVER["HTTP_X_FORWARDED_FOR"]);
	}elseif(!empty($_SERVER["REMOTE_ADDR"])){
		$cip = ip2long($_SERVER["REMOTE_ADDR"]);
	}else{
		$cip = 0;
	}
	return $cip;
}