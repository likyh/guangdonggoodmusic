<?php
function jsondEcho(&$result){
    $result['serverTime']=$_SERVER['REQUEST_TIME'];
    $result['serverDate']=date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']);
    echo json_encode($result);
}
//公共函数
function get_data($url, $timeout = 3, $jsonp=false){
    if(empty($url)) return null;
    if($jsonp){
        $_options = stream_context_create(array(
                'http' => array(
                    'timeout' => $timeout,
                    'header'=>"X-Requested-With: XMLHttpRequest\r\n"
                ))
        );
    }else{
        $_options = stream_context_create(array(
                'http' => array(
                    'timeout' => $timeout
                ))
        );
    }
    $data = file_get_contents($url, 0, $_options);
    return $data;
}