<?php
// sae使用这一句话初始化
$mmc=memcache_init();

// 普通服务器使用这一句话初始化
//$mmc = memcache_connect("127.0.0.1:12345");

/**
 * @param $key
 * @return mixed
 */
function getCache($key){
    global $mmc;
    return memcache_get($mmc, $key);
}
function setCache($key, $value){
    global $mmc;
    return memcache_set($mmc, $key, $value);
}