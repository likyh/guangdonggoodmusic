<?php
include_once 'SqlDB.class.php';
include_once 'common.php';
class Data {
    /**
     * @var SqlDB
     */
    protected $db;
    function __construct(){
        $this->db= SqlDB::init();
    }

    /**
     * 获取歌曲详情(歌名，歌手，歌手头像，个人简介，创作灵感，作品介绍，上传时间，阶段)
     * @param $id   歌曲id
     * @return array()[★肖瑜看返回$result['music']、$result['preId']、$result['nextId']★]
     */
    public function getOneWorks($musicId,$curperiod){
        $musicId=(int)$musicId;
        $selectSql="SELECT * FROM `music` WHERE `id`=$musicId";
        $result['music']=$this->db->getOne($selectSql);
        $period=(int)$curperiod;        //当前阶段
        $total=$this->db->getValue("select count(*) from `music` where `period`=$period");
                //总计
        //获取该篇作品相对于票选排名顺序的上一篇和下一篇
        $thisPeriodAll="SELECT `id` FROM `music` WHERE `period`=$period order by vote DESC";
        $re=$this->db->getAll($thisPeriodAll); 
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$musicId){
                if($i==0){
                    $result['preId']=NULL;
                    $result['nextId']=$re[$i+1]['id'];
                }else if($i==$total-1){
                    $result['preId']=$re[$i-1]['id'];
                    $result['nextId']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['nextId']=$re[$i+1]['id'];
                }
            }
        } 
        //获取该作品歌手的所有作品id
        $singer=$this->db->getValue("SELECT `singer` from `music` where `id`=$musicId");
        $otherWorksSql="SELECT `id`,`music_url`,`pic_url`,`video_url`,`period` FROM `music` where `singer`='$singer' order by `period` DESC";
        $result['allMusic']=$this->db->getAll($otherWorksSql);
        return $result;
    }
    
    /**
     * 获取歌曲详情(歌名，歌手，歌手头像，个人简介，创作灵感，作品介绍，上传时间，阶段)
     * @param $id   歌曲id
     * @return string
     */
    public function getAllWorks(){
        $selectSql="SELECT id,title,singer,vote,pic_url FROM `music`";
        $result=$this->db->getAll($selectSql);
        return $result;
    }
    /**
     * 获取歌曲作品总数(歌名，歌手，歌手头像，个人简介，创作灵感，作品介绍，上传时间，阶段)
     * @param $id   歌曲id
     * @return string
     */
    public function getWorksNumbers(){
        $selectSql="SELECT count(*) FROM `music`";
        $result=$this->db->getValue($selectSql);
        return $result;
    }

    /**
     * 获取新闻报道列表（只有几条，只显示标题时间的）（ID，歌名，上传时间）
     * @param 无
     * @return string
     */
    public function getNewsListRough(){
        $selectSql="SELECT `id`, `title`, `create_time` FROM `news` order by `id` desc limit 3";
        $result=$this->db->getAll($selectSql);
        return $result;
    }
    /**
     * 获取详细新闻报道列表(ID，歌名，内容，上传时间)
     * @param 无
     * @return string
     */
    public function getNewsListDetailed($newsId){
        $newsId=(int)$newsId;
        $selectSql="SELECT * FROM `news` where id=".$newsId;
        $result=$this->db->getOne($selectSql);
        return $result;
    }
     /**
     * 获取其他新闻报道列表(ID，歌名，内容，上传时间)
     * @param 无
     * @return string
     */
    function getOtherDetail($newsId){
     $newsId=(int)$newsId;   
     $sql="select * from news where id not in (".$newsId.") order by create_time desc limit 6 ";
     $db=$this->db;
     $result=$db->getAll($sql);
     return $result;
    }
    /**
     * 获取一篇新闻内容
     * @param $newsId
     * @return string
     */    
//    public function getNewsContent($newsId){
//      $newsId=(int)$newsId;
//      $selectSql="SELECT `title`,`content`,`create_time` FROM `news` WHERE `id`= order by `id`=$newsId DESC";
//      $result=$this->db->getOne($selectSql);
//      return $result;
//    }

    public function getPeriod(){
         return $this->db->getValue('select `period` from `cur_period` limit 1');
    }
    
    /**
     * 获取作品列表  某阶段中晋级的（ID,歌名，歌手，票数）
     * @param $period 某个阶段的
     * @return string
     */
    public function getPartWorksList($period){
        if(isset($period)){
            $period=(int)$period;
        }else{
            $period=$this->getPeriod();
        }       
        $selectSql="SELECT `id`,`title`, `singer`, `vote` FROM `music` WHERE `period`=$period order by vote DESC";
        $result=$this->db->getAll($selectSql);
        return $result;
    }
    /**
     * 获取所有作品列表（所有阶段）--按照通票方式（ID,歌名，歌手，票数）
     * @param 无 
     * @return array 一个2维数组
     */
    public function getAllWorksList(){
        $selectSql="SELECT `id`,`title`, `singer`, `vote` FROM `music` order by vote DESC";
        $result=$this->db->getAll($selectSql);
        return $result;
    }

     /**
     * 所有作品列表fenye（所有阶段）--按照上传时间（ID,歌名，歌手，票数）
     * @param 无 
     * @return array 一个2维数组
     */
    function pagingBycreate_time($currentpage,$pagesize,$period){
        $currentpage=(int)$currentpage;
        $pagesize=(int)$pagesize;
        $period=(int)$period;
        $offset = ($currentpage-1)*$pagesize;
        $query="select * from music where period={$period} order by create_time desc limit $offset,$pagesize";
        $result= $this->db->getAll($query);
        return $result;
    }
         /**
     * 所有作品列表fenye（所有阶段）--按照上传时间（ID,歌名，歌手，票数）
     * @param 无 
     * @return array 一个2维数组
     */
    function pagingByVote($currentpage,$pagesize,$period){
       $currentpage=(int)$currentpage;
       $pagesize=(int)$pagesize;
       $period=(int)$period;
       $offset = ($currentpage-1)*$pagesize;
       $query="select * from music where period={$period}  order by vote desc limit $offset,$pagesize";
       $result= $this->db->getAll($query);
       return $result;
    }
     /**
     * 作品排行榜页面fenye（所有阶段）--按照上传时间（ID,歌名，歌手，票数）
     * @param 无 
     * @return array 一个2维数组
     */
    function workRankpaging($currentpage,$pagesize){
        $currentpage=(int)$currentpage;
        $pagesize=(int)$pagesize;
        $period=$this->getPeriod();
        $offset = ($currentpage-1)*$pagesize;
        $query="select * from music where period={$period} order by vote desc limit $offset,$pagesize";
        $result= $this->db->getAll($query);
        return $result;
    }
    function finalVote(){
        $result['vote1']=$vote1=$this->db->getValue("select `vote` from music where `id`='2453311'");
        $result['vote2']=$vote2=$this->db->getValue("select `vote` from music where `id`='2452376'");
        $result['vote3']=$vote3=$this->db->getValue("select `vote` from music where `id`='2454294'");
        return $result;
    }
    /**
     * 作品评论
     * @param $musicId;
     * @return string 评论总数，每一条评论
     */
    public function getComment($musicId){
        $period=(int)$musicId;
        $countSql="SELECT count(1) as total FROM `comment` WHERE `music_id`=$musicId";
        $result['total']=$this->db->getValue($countSql);
        $selectSql="SELECT `username`, `content`, `timestamp` FROM `comment` WHERE `music_id`=$musicId order by timestamp  DESC";
        $result['comment']=$this->db->getAll($selectSql);
        return $result;
    }    
    /**
     * 提交评论
     * @param $username,$content,$musicId; 用户名，内容，歌曲id
     * @return true or false
     */
    public function addComment($username,$content,$musicId){
        $musicId=(int)$musicId;
        $username=$this->db->quote($username);
        $content=$this->db->quote($content);
        $sql="INSERT INTO `comment` (`music_id`,`username`,`content`)VALUES({$musicId},{$username},{$content})";
        if($this->db->sqlExec($sql)){
            return true;
        }else{
            return false;
        }
    }

    public function commentPaging($currentpage,$pagesize,$musicId){
       $currentpage=(int)$currentpage;
       $pagesize=(int)$pagesize;
       $musicId=(int)$musicId;        
       $offset = ($currentpage-1)*$pagesize;
       $query="select * from comment where music_id=$musicId order by timestamp desc limit $offset,$pagesize";   
       $result= $this->db->getAll($query);
       return $result;
    }
    
    public function getMusicIdByText($text,$period){
        $db=$this->db;
        $value=$db->quote("%{$text}%");
        $period=(int)$period;
        $sql="SELECT * FROM `music` where `period`={$period} and (`title` like $value or `singer` like $value ) order by `create_time`";    
        if($db->getExist($sql)){
            return $db->getAll($sql);
        }else{
            return 0;
        }
    }
   
   
     /**
     * 排行榜（某一阶段）（ID,歌名，歌手，票数）
     * @param 无 
     * @return string
     */
    public function getRank(){
        $period=$this->getPeriod();
        $selectSql="SELECT `id`,`title`, `singer`, `vote`, `pic_url` FROM `music` WHERE `period`=$period order by vote DESC limit 0,3";     
        $result=$this->db->getAll($selectSql);
        return $result;
    }  
    
     public function getRank2(){
        $period=$this->getPeriod();
        $selectSql="SELECT `id`,`title`, `singer`, `vote`, `pic_url` FROM `music` WHERE `period`='$period' order by vote DESC limit 3,3";       
        $result=$this->db->getAll($selectSql);
        return $result;
    } 
    
       
    
    /**
     * @param $musicId （默认当前阶段的）
     * @return array  返回歌曲的票数和排名
     */
    public function voteRank($musicId,$curperiod){
        $musicId=(int)$musicId;
        $period=(int)$curperiod;
        $total=$this->db->getValue("select count(*) from `music` where `period`=$period");
        $sql="select `vote`,`id`,`score` from `music` where `period`=$period order by `vote` DESC";
        $re=$this->db->getAll($sql);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$musicId){
                $result['vote']=$re[$i]['vote'];
                $result['score']=$re[$i]['score'];
                $result['rank']=$i+1;
                return $result;
            }
        }
        return null;
    }
    public function getNotice(){
        $sql="SELECT `content` FROM `notice` order by `create_time` desc limit 1";
        $result=$this->db->getValue($sql);
        return $result;
    }
    public function getSixNewsList(){
        $sql="SELECT `id`,`title`,`content`,`picurl`,`create_time` from `news` order by `create_time` DESC limit 6";
        $result=$this->db->getAll($sql);
        return $result;
    }        
 }  
