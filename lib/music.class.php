<?php
include_once 'SqlDB.class.php';
include_once 'common.php';
class Music {

    /**
     * @var SqlDB
     */
    protected $db;
    function __construct(){
        $this->db= SqlDB::init();
    }

    public function getPeriod(){
        return $this->db->getValue('select `period` from `cur_period` limit 1');
    }

    function refresh($api,$period){
        $newData=json_decode(get_data($api),true);
        $newMusicArray=$newData['songlist'];
        $period=(int)$period;
        // 更新数据
        if(!isset($newData['songlist'])){
            return false;
        }
        foreach($newMusicArray as $key=>$value){
            $p=$period;
            if($period==1) $p=0;
            $musicId=(int)$value['SI']+$p;
            $data['musicId']=$musicId;
            $data['title']=$value['SN'];
            $data['singer']=$value['RN'];
            $data['introduction']=$value['M'];
            $data['inspiration']=$value['C'];//作品灵感
            $data['pic_url']=(int)$p==0?'':$value['VI'];//视频略缩图
            $data['video_url']=(int)$p==0?'':$value['VU'];  
            //$data['music_url']=$value['FN'];  //音频
            $data['photo']=$value['I'];//用户头像
            $data['profile']=$value['M'];//个人简介
            $data['create_time']=isset($value['CT'])?$value['CT']:'';  
            $data['email']=$value['E']; 
            $data['tel']=$value['P']; 
            $data['period']=$period;                  
            if(!$this->musicExist($musicId)){            
                $this->musicInsert($data);                
            }else{                                      
                $this->musicUpdate($data);             
            }
        }
        return true;
    }

    public function musicExist($musicId){
        $musicId=(int)$musicId;
        return $this->db->getExist("SELECT `id` FROM `music` WHERE `id`=$musicId");
    }

    public function musicInsert($data){
        foreach($data as $k=>$v){
            $data[$k]=$this->db->quote($v);
        }
        $sql=<<<insertSql
INSERT INTO `music`
(`id`,`title`,`singer`,`pic_url`,`video_url`,`inspiration`,`introduction`,
`create_time`,`period`,`photo`,`profile`,`email`,`tel`)VALUES
({$data['musicId']},{$data['title']},{$data['singer']},{$data['pic_url']},{$data['video_url']},{$data['inspiration']},{$data['introduction']},{$data['create_time']},{$data['period']},
{$data['photo']},{$data['profile']},{$data['email']},{$data['tel']});
insertSql;
        return $this->db->sqlExec($sql)>0;
    }
    
    public function musicUpdate($data){
        foreach ($data as $k => $v) {
            $data[$k]=$this->db->quote($v);
        }
        $sql=<<<updateSql
UPDATE `music` SET `video_url`={$data['video_url']},`pic_url`={$data['pic_url']},
`inspiration`={$data['inspiration']},`introduction`={$data['introduction']},
`period`={$data['period']},`photo`={$data['photo']},`profile`={$data['profile']},`email`={$data['email']},`tel`={$data['tel']} WHERE `id`={$data['musicId']};
updateSql;
    return $this->db->sqlExec($sql)>0;
    }
} 