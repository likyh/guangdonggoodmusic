<?php
 include_once 'SqlDB.class.php';
 class GameReport{
    protected $db;
    function __construct(){
        $this->db= SqlDB::init();
    }
     /**
     * 获取总共的新闻个数
     * @param 无
     * @return string
     */
	function getRows($sql){
	  $db=$this->db;
	  $sth=$db->getValue($sql) ;
      return $sth;
	}
	
	/**
     * 获取works_rank页面的新闻个数（根据当前阶段）
     * @param 无
     * @return string
     */
	function getWordsRankRowsByPeriod(){ 
	  $db=$this->db;
	  $period=$db->getValue("select `period` from `cur_period` limit 1");
	   $sql="select count(*) from music where period={$period}";
	  $sth=$db->getValue($sql) ;
      return $sth;
	}
	
	  /**
     * 获取展示新闻需要几个页面
     * @param 新闻总数，每页多少条新闻
     * @return int 
     */
	function getPages($rows,$pagesize){
		$rows=(int)$rows;
		$pagesize=(int)$pagesize;
	  $pages=ceil($rows/$pagesize);
	  return $pages;	  
	}
	  /**
     * 获取每个页面要展示的新闻
     * @param 当前页，每页多少条新闻
     * @return 2维数组 
     */
	function paging($currentpage,$pagesize){
		$currentpage=(int)$currentpage;
		$pagesize=(int)$pagesize;
	 $offset = ($currentpage-1)*$pagesize;
	 $query="select * from news order by create_time desc limit $offset,$pagesize";
	 $result= $this->db->getAll($query);
	 return $result;
	}
 } 
