<?php
include "check.php";
?><html>
<head>
	<meta charset="utf-8"  name="keywords" content="广东好歌比赛进程，广东好歌评选机制，广东好歌报名方式，广东好歌报名条件">
	<meta charset="utf-8" name="description" content="广东好歌选秀大赛官方网站为您提供广东好歌比赛进程，广东好歌参赛规则，报名条件及评分机制，让您更清楚的了解广东好歌。" >
	<title>【活动说明】比赛进程_参赛规则_报名条件_评分机制-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/activityIntro.css"/>
    <link rel="stylesheet" type="text/css" href="style/act.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="shuoming.php";
		pn="activity";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/activity.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
   
</head>

<body>
<!--BEGIN #container-->
<div id="container">
  <div id="header">
     <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter">
      <?php include 'part/personalCenter.php'; ?>
    </div>
    <div id="title">
      <div class="button">
          <?php include_once "part/navigation.php";?>
      </div>
    </div>
  </div>
    <div id="content" class="activityIntroshengming">
        <div id="share">
            分享活动到：
          <a target="_blank" class="bds_tsina" title="分享到新浪微博" href="http://v.t.sina.com.cn/share/share.php?url=http%3a%2f%2fgdhg.kugou.com%2fshuoming.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'新浪微博'});"><img src="style/images/sinaimg.png"/></a>
            <a target="_blank" class="bds_renren" title="分享到人人网" href="http://widget.renren.com/dialog/share?resourceUrl=http://gdhg.kugou.com/shuoming.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=广东好歌&content=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'人人'});"><img src="style/images/renren.png"/></a>
            <a target="_blank" class="bds_douban" title="分享到豆瓣网" href="http://www.douban.com/recommend/?url=http%3a%2f%2fgdhg.kugou.com%2fshuoming.php%3futm_source%3dsocialshare%26utm_medium%3ddouban%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'豆瓣'});"><img src="style/images/douban.png"/></a>
            <a target="_blank" class="bds_tengxun" title="分享到腾讯微博" href="http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fgdhg.kugou.com%2Fshuoming.php%3Futm_source%3Dsocialshare%26utm_medium%3Dtencent%26utm_campaign%3D5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'腾讯微博'});"><img src="style/images/tengxun.png"/></a>
        </div>
        <div class="navigationButton1">
            <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
        </div>
        <article>
        <!--活动说明-->
  <div id="actIll">
  <img src="style/images/navline.png">
    <h4>和你爱的广东有个粤会</h4>
    <p> 每一个出生、成长、生活、或喜欢广东的人，对她有着非同一般的情感。一千个人有一千种对广东的认识，有没有一首歌代表你<br>
      心目中的广东？承载纯正广东文化的广东知名雪糕品牌五羊，携全球知名音乐公司索尼音乐娱乐、国内顶尖数字音乐平台酷狗音乐,
      为每个热爱广东文化的你联手打造广东原创歌曲网络比赛，以新颖好玩的方式，用音乐和你爱的广东来个粤会吧！
    </p>
    <h4>粤唱时间:</h4>
    <img src="style/images/evenrepresentimg.png"/>
    <div class="whitetxt">
      <h4>参赛规则：</h4>
      <p>只要你热爱广东文化，不限年龄、性别、居住所在地，不限参赛形式，无论是个人、组合、乐队等多种形式，均可以参加，<br>
        唱出心目中的广东。</p>
      <p><span>歌曲主旨：</span>以弘扬广东文化为主旨，内容需包含与广东文化相关的元素</p>
      <p><span>唱词要求：</span>唱词需全部或部分包含粤语元素。</p>
      <p><span>作品长度：</span>参赛者需上传完整的原创作品，或至少该作品的一个完整段落。</p>
      <p><span>上传格式：</span>海选阶段只需上传MP3格式的作品，第二轮及第三轮比赛需提供视频格式文件。</p>
      <p><span>评分机制：</span>由网络观众投票+专业评审团打分两部分综合评选。</p>
      <h4><span>评分机制：</span></h4>
      <p>【网友投票数*平均评委星星数】= 作品总分</p>
      <p>　每一位评委有五颗星，以半颗星为间隔，划分为10个标准（0.5颗星，1颗星，1.5颗星 … 5颗星）</p>
      <p>　例如：【6,400票数*3.5颗星】 = 22,400总分</p>
      <h4>网友投票:</h4>
      <p>使用编码（从五羊甜筒盖下）可获得五票。</p>
      <p>五票只能投给同一名选手</p>
      <p>每天允许使用多个编码</p>
        
      <h4>专业评审团评分标准:</h4>
      <p>第一轮：上传自创音频，词曲分各占50%</p>
      <p>第二轮：上传自制视频，词曲分各占45%，视频质量10%</p>
      <p>第三轮：上传自制视频，词曲分各占45%，视频质量10%</p>
      <p>决  赛：依据选手现场表演，由专业评审团及粉丝酷评团打分</p>

    </div>
  </div>
  </article>
    </div>
</div>
<?php include_once "part/footer.php"; ?>
<!--END #container-->
</body>
</html>
