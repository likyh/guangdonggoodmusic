<div class="site-msg">
	<?= $message; ?>
	<?php if (!empty($redirect)): ?>
		<?= CHtml::link('点击此处马上跳转'.(!empty($delay) ? sprintf('(%s秒后自动跳转)', $delay) : ''), $redirect); ?>
	<?php endif; ?>
</div>
