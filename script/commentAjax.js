/*$(document).ready(function(){*/
var curPage = 1; //当前页码 
var total,pageSize,totalPage; //总记录数，每页显示数，总页数

$(document).ready(function(){
	$(('.'+curPage)).addClass('page_active');
	$("#prevPageAjax,#nextPageAjax,#commentPageAjax").click(function(){
	   $("#prevPageAjax,#nextPageAjax,#commentPageAjax").removeClass('page_active');	
	   musicId=$(this).attr("data-id");
	   period=$(this).attr("data-period");
       page=$(this).attr("data-page");  
	   curPage='.'+page;
	   $(curPage).addClass('page_active');
	   $.ajax({
            type: "get",
            url: "commentAjax.php?musicId="+musicId+"&period="+period+"&page="+page,
            dataType: "json",		
            success: function (data) {  
                    var result = eval(data);
					var htmlstr ='';
		 $.each(result, function (index, value) {  
		      htmlstr+="<h4><span id='name'>"+ value['username']
					   +"</span>发布于<span id='commenttime'>"+value['timestamp']
					   +"</span></h4><p>"+value['content']
					   +" </p>";	
          });  
				   $("#allcommentsWithoutTitle").html(htmlstr);  
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {       
            	alert(XMLHttpRequest.readyState + XMLHttpRequest.status + XMLHttpRequest.responseText);  
            }
        });		      
	});
}); 
 

//获取分页条 
function getPageBar(){ 
    //页码大于最大页数 
    if(curPage>totalPage) curPage=totalPage; 
    //页码小于1 
    if(curPage<1) curPage=1; 
    pageStr = "<span>共"+total+"条</span><span>"+curPage 
    +"/"+totalPage+"</span>"; 
     
    //如果是第一页 
    if(curPage==1){ 
        pageStr += "<span>首页</span><span>上一页</span>"; 
    }else{ 
        pageStr += "<span><a href='javascript:void(0)' rel='1'>首页</a></span><span><a href='javascript:void(0)' rel='"+(curPage-1)+"'>上一页</a></span>";
    } 
     
    //如果是最后页 
    if(curPage>=totalPage){ 
        pageStr += "<span>下一页</span><span>尾页</span>"; 
    }else{ 
        pageStr += 
		"<span><a href='javascript:void(0)' rel='"+(parseInt(curPage)+1)+"'> 下一页</a></span><span><a href='javascript:void(0)' rel='"+totalPage+"'>尾页</a></span>"; 
    } 
         
    $("#page").html(pageStr); 
} 