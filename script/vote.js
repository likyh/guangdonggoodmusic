/**
 * Created by linyh on 14-3-18.
 */
$(document).ready(function(){
    // 直接投票、唯一码投票
    var $float=$("#float");
    var $layer;
    $float.hide();
	var userOpenId=getCookie('userOpenId');
	$(document).on('click','.voted,.voteDivClick,.videorightClick',function(){
        alert("比赛已经结束");
        return false;
        musicId=$(this).attr("data-id");
		period=$(this).attr("data-period");
        $num=$(this).parent().parent().find(".num,#votenum");
        dataLayer.push({'event':'event','cat':'投票','act':'投票','lbl':musicId});
        $layer = $.layer({
            type: 1,
            title: false,
            closeBtn: false,
            border : [0, 0, '#666', true],
            bgcolor :null,
            area: ['720px','386px'],
            page: {
                dom : '#float'
            },
            success: function(){
                dataLayer.push({'event':'page','branch':'投票','section':'投票页面','pname':''});
            }
        });
		$float.find(".uniqueCode h4").html("输入编码为选手投票!<br>iPad好礼等你赢");
		$float.find(".uniqueCode h4").removeClass('messageColor');
		$float.find(".uniqueCode p").html("从促销装甜筒盖下获得编码。");
		$float.css("background-image","url(style/images/popupbg.png)");
		$float.find(".qcode span").html("");
        return false;
    });

    $("#sureVote").click(function(){
        dataLayer.push({'event':'event','cat':'投票','act':'确认投票','lbl':musicId});
        var uniqueCode=$("#uniqueCodeInput").val();
        $float.find(".uniqueCode h4").html("提交中<br/>请稍等，投票信息提交中");
        $float.find(".uniqueCode p").html("");
        $float.find(".uniqueCode h4").removeClass('messageColor');
        $float.find(".qcode span").html("");
        var exitTime = new Date().getTime() + 800;
        if(uniqueCode!=""){
            $.ajax({
            type: "get",
            url: "voteAction.php?musicId="+musicId+"&uniqueCode="+uniqueCode+"&period="+period,
            dataType: "json",
            success: function (data) {
                while(new Date().getTime()< exitTime){}
                if(data.message=='success'){
                    $float.find(".uniqueCode h4").html("投票成功!<br>继续为当前好歌投票！");
                    $float.find(".uniqueCode p").html("");
                    $float.find(".qcode span").html("<a href='lottery.php' onclick=\"dataLayer.push({'event':'event','cat':'投票成功','act':'点击抽奖','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;\"> 现在就参与抽奖赢iPad> </a>");
                    $float.find(".uniqueCode h4").removeClass('messageColor');
                    $float.css("background-image","url(style/images/popupsuccessbg.png)");

                    $num.text(parseInt($num.text())+5+"票");
                    dataLayer.push({'event':'event','cat':'投票','act':'编码','lbl':musicId});
                }else{
                    $float.find(".uniqueCode h4").html(data.message);
					$float.find(".uniqueCode p").html("从促销装甜筒盖下获得编码，重新输入。");
					$float.find(".uniqueCode h4").addClass('messageColor');
					$float.find(".qcode span").html("");
                    // $float.find(".uniqueCode p").html(data.message);
                }
                $("#uniqueCodeInput").val("");
            }
            });
        }else{
            alert("请输入编码!");
        }
        return false;
    });
    $float.find("#close").click(function(){
        layer.close($layer);
       /* $float.find(".message h4").text("提交中");
        $float.find(".message p").html("正在提交您的请求，请稍等……");*/
        return false;
    });
});

function getCookie(Name) 
{ 
    var search = Name + "=" 
    if(document.cookie.length > 0) 
    { 
        offset = document.cookie.indexOf(search) 
        if(offset != -1) 
        { 
            offset += search.length 
            end = document.cookie.indexOf(";", offset) 
            if(end == -1) end = document.cookie.length 
            return unescape(document.cookie.substring(offset, end)) 
        } 
        else return "" ;
    } 
}