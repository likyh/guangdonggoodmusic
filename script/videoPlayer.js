$(document).ready(function(){
    var params = {
        menu: "false",
        scale: "noScale",
        allowFullscreen: "true",
        allowScriptAccess: "always",
        bgcolor: "#fff",
        quality: "high",
        wmode: "opaque"
    };
    var attributes = {
        id:"videoPlayer"
    };

    function updateFlashPlayer($a, autoPlay){
        var $url;
        var $musicId=$a.attr("data-id");
        var period=Number($a.attr("data-period"));
        var periodTemp;
        switch(period){
            case 1:periodTemp="海选作品";period=0;break;
            case 2:periodTemp="五十强作品";break;
            case 3:periodTemp="二十五强作品";break;
            case 4:periodTemp="复活赛作品";break;
            case 5:periodTemp="决赛作品";break;
        }

        var $image=$a.find("img");
        if($a.attr("data-v")){
            $url=$a.attr("data-v");
            $playerUrl="http://static.5sing.com/js/video/videoPlayer.swf?url="+$url+"&autoplay=true&autohide=true";
            $playerUrl="http://static.kugou.com/common/swf/video/skin.swf&amp;aspect=true&amp;url="+$url+"&amp;autoplay=true&amp;fullscreen=true";
            var $t=$('<object id="webmvplayer" name="webmvplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10.0.32" width="404" height="266"><param name="bgColor" value="#666666"><param name="movie" value="http://static.kugou.com/common/swf/video/videoPlayer.swf"><param name="flashvars" value="skinurl='+$playerUrl+'"><param name="quality" value="high"><param name="allowScriptAccess" value="always"><param name="WMODE" value="transparent"><param name="allowFullScreen" value="true"><embed name="jam" src="http://static.kugou.com/common/swf/video/videoPlayer.swf" width="404" height="266" allowscriptaccess="always" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="skinurl='+$playerUrl+'" type="application/x-shockwave-flash" wmode="transparent" allowfullscreen="true"></object>');
            $("#videoPlayer").html($t);
            $(".videoPic").hide();
        }else if(autoPlay){
            $url=$a.attr("data-m");
            var $a=$.layer({
                type : 2,
                title: "播放音乐",
                iframe : {src : 'http://l.5sing.com/player.swf?autostart=true&songtype=yc&songid='+(Number($musicId)-Number(period))},
                area : ['250px' , '68px'],
                border : [20, 0.5, '#666', true],
                offset : ['200px',''],
                close : function(index){
                    layer.close($a);
                }
            });
            $("#videoPlayer").html("");
            $(".videoPic").show();
        }
    }

    // 设置默认视频
    var $a=$(".videoPeriod a:first");
    $a.addClass("active");
    updateFlashPlayer($a,false);


    $(".videoPeriod a").click(function(){
        $(".videoPeriod a").removeClass("active");
        var $a=$(this);
        $a.addClass("active");
        updateFlashPlayer($a,true);
        return false;
    });
});