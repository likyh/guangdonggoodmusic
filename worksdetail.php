<?php
include_once "check.php";
include_once "lib/data.class.php";
include_once "lib/news.class.php";
include_once "lib/imageURL.php";

if (isset($_GET['musicId'])) {
    $musicId = (int)$_GET['musicId'];
	if(isset($_GET['period'])){
	 $curperiod=(int)$_GET['period'];
	}else{
     $curperiod=1;
    }
}
$data = new Data();
$imgUrl=new ImageUrl;
/*票选排名*/
$rankNumber=$data->voteRank($musicId,$curperiod);
$voterank=$rankNumber['rank'];
$score=$rankNumber['score'];
/* 当前时期*/
$period=$data->getPeriod();
/* 获取作品信息*/
$resultsetMusic = $data->getOneWorks($musicId,$curperiod);
$resultset=$resultsetMusic['music'];
$resultallMusic=$resultsetMusic['allMusic'];
/* 上一首、下一首*/
if(isset($resultsetMusic['nextId'])&&!empty($resultsetMusic['nextId']))$nextSong =$resultsetMusic['nextId'];
if(isset($resultsetMusic['preId'])&&!empty($resultsetMusic['preId']))$prevSong =$resultsetMusic['preId'];
/* 评论分页*/
/*$gr = new GameReport();
$pagesize =3;
$musicId=(int)$musicId;
$sql = "select count(*) from comment where music_id=".$musicId;
$rows = $gr->getRows($sql);
if ($rows > 0) {
    $pages = $gr->getPages($rows, $pagesize); //一共有多少页
    $currentpage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    if ($currentpage < 1) {
        $currentpage = 1;
    } else if ($currentpage > $pages) {
        $currentpage = $pages;
    }

    $prevPage = $currentpage - 1 > 0 ? $currentpage - 1 : 1;
    $nextPage = $currentpage + 1 <= $pages ? $currentpage + 1 : $pages;

    $pageresultset = array();
    $pageresultset = $data->commentPaging($currentpage, $pagesize, $musicId);

} else {
    $pageresultset = null;
}*/
/* 作品排行*/
$rankResultset = $data->getRank();
$rankResultset2 = $data->getRank2();
?><!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8">
    <meta  name="keywords" content="<?php echo $resultset['title'];?>">
	<meta name="description" content="<?php echo mb_substr(htmlentities(strip_tags($resultset['inspiration']),ENT_QUOTES,'UTF-8'),0,200,'utf8');?>" >
	<title>参赛作品详情标题-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/worksdetail.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="worksdetail.php";
		pn="works";
		params=window.location.search;
		pageName+=params;
		if(getCookie('userOpenId')!=""){
			 userId=getCookie('userOpenId');
		}else{
			 userId="";
		}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script> 
    <script type="text/javascript" src="script/layer.min.js"></script>
    <script type="text/javascript" src="script/videoPlayer.js"></script>
    <script type="text/javascript" src="script/vote.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
				var curperiod=$("#curperiod").val();
				var musicId=$("#musicId").val();
                loadData(musicId,curperiod,1);  // For first time page load default results
				 $(document).on('click','.allcomments #page span.active',function(){
                    var page = $(this).attr('p');
                    loadData(musicId,curperiod,page);
                });           
		}); 
		function loadData(musicId,period,page){
        	$.ajax({
            	type: "get",
                 url: "commentAjax.php?musicId="+musicId+"&period="+period+"&page="+page,                      
                 success: function(msg){
					msg=unescape(msg); 
                 	$(".allcomments").html(msg);
                 },
				 error: function (XMLHttpRequest, textStatus, errorThrown) {       
            		alert(XMLHttpRequest.readyState + XMLHttpRequest.status + XMLHttpRequest.responseText);  
            	 }
             });
         } 
	</script>
    <script type="text/javascript" src="script/comment.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
    
</head>

<body>
<!--BEGIN #container-->
<div id="container"> 
	<div id="header">
		<div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    	<div id="personalCenter"><?php include_once 'part/personalCenter.php'; ?></div>
    	<div id="title">
     	 <div class="button"><?php include_once "part/navigation.php";?></div>
		</div>
	</div> 
    <div id="content" class="worksdetails">
		<div id="detailsleft">
			<div class="video">
            <div class="videoleft">
            <?php $title=$resultset['title']; 
				if(mb_strlen($title,"utf8")>15){				
				 echo "<h3>".$title."</h3>";
				}else{
				 echo "<h3>".$title."</h3>";
				}?>
                 <div id="rankandgrade">票选排名:<span class="voteranking"><?php echo $voterank;?></span>评委评分:<?php echo $score;?><span class="judgegrade"></span></div>
               </div>
               
                 <?php if($curperiod==$period){ ?> 
               <div class="videoright videorightClick" data-period="<?php echo $curperiod; ?>" data-id="<?php echo $resultset['id']; ?>">
                <a href="#" id="voteButton" ></a>
                <span class="votenum" id="votenum"><?php echo $resultset['vote']?>票</span>
               </div>
                <?php }else{ ?>
               <div class="videoright">
                <a href="#" id="voteButton"></a>
                <span class="votenum" id="votenum"><?php echo $resultset['vote']?>票</span>
               </div>
               <?php } ?> 
                <div class="videoplayarea" >
                	<div id="videoPlayer" style="z-index:10;background:none"></div>
					<img class="videoPic" width="404" height="266" src="<?php echo $resultset['pic_url']?>" />
                </div>
                <div class="videoPeriod" id="videoPeriod">
                   <span><?php echo $resultset['singer']?>的参赛作品</span>
                   <ul>
                    <?php foreach($resultallMusic as $rm) {
                        switch($rm['period']){
                            case 1:$periodTemp="海选作品";break;
                            case 2:$periodTemp="五十强作品";break;
                            case 3:$periodTemp="二十五强作品";break;
                            case 4:$periodTemp="复活赛作品";break;
                            case 5:$periodTemp="决赛作品";break;
                            case 6:$periodTemp="获奖作品";break;
                        }?>
                        <li>
                        <a href="#" data-v="<?php echo $rm['video_url']?>" data-m="<?php echo $rm['music_url']?>" data-id="<?php echo $rm['id']?>" data-period="<?php echo $rm['period']?>">
                            <img src="<?php echo $rm['pic_url']?>" /><span class="periodWork"><?php echo $periodTemp;?></span></a>
                       </li>
                    <?php } ?>   
                   </ul>
                 </div>
                <h4>创作灵感:</h4>
                <p><span><?php echo $resultset['inspiration']?></span></p>
                <div id="share">分享作品到:<?php include "part/shareLink.php";?></div>
                <div id="preandnext">
					<?php if(isset($prevSong)&&!empty($prevSong)){ ?>
                     <a href="worksdetail.php?musicId=<?php echo $prevSong;?>&period=<?php echo $curperiod;?>" class="presong"></a><?php } ?> 
                   <?php if(isset($nextSong)&&!empty($nextSong)){ ?>   
                     <a href="worksdetail.php?musicId=<?php echo  $nextSong;?>&period=<?php echo $curperiod;?>" class="nextsong"></a><?php } ?> 
                </div>
             </div>
             <!-- comment -->
             <div class="comment">
               <!-- 评论区域-->
                <div class="commtextarea">
                    <span id="textCount"></span>
                    <div id="textandcomment">
                        <textarea name="comments" rows="2" cols="42" id="TextArea1"></textarea>
                        <input name="" type="hidden" value="<?php echo $resultset['id']?>" id="m_id"/>
                        <a href="#"  class="iwantcomment" id="submitButton" data-period=<?php echo $curperiod;?>></a>
                    </div>
                </div>
                <!--All comment -->
                <div class="allcomments">
                	<input type="hidden" id="curperiod" value="<?php echo $curperiod;?>">
                    <input type="hidden" id="musicId" value="<?php echo $musicId;?>">
                   <div id="singleComment"></div>
                   <div id="page"></div>
                </div>
            </div>
        </div>
        <div id="detailsright">
			<div class="studio">
				<h3>姓名:<span id="playername"><?php echo $resultset['singer']?></span></h3>
                <img src="<?php echo $resultset['photo'] ?>" id="singerPic"/>
                <p>选手介绍:<span><?php echo $resultset['introduction'];?></span></p>
               <!-- <a  href="<?php echo $resultset['fx_url'];?>" onClick="dataLayer.push({'event':'event','cat':'参赛作品','act':'作品详情-<?php echo $resultset['singer'] ?>','lbl':'进入直播室'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">进入直播室</a>-->
            </div>

            <div class="chart">
                <h2>参赛歌曲</h2>
                <ul>
                <?php
                $i = 0;
               if($rankResultset!=null){ foreach ($rankResultset as $rankresult) {
                    $i++;?>
                    <li class="top">
                        <span class="ranknum"><?php echo $i ?></span>
                        <a href="worksdetail.php?musicId=<?php echo $rankresult['id']?>&period=<?php echo $period;?>"> 
                        <img src="<?php $pic_url=$rankresult['pic_url']; $picurl=$imgUrl->changeUrl($pic_url,"_113_64");echo $picurl; ?>"/></a>
                        <h3><a href="worksdetail.php?musicId=<?php echo $rankresult['id']?>&period=<?php echo $period;?>"><?php $title=$rankresult['title']; 
							if(mb_strlen($title,"utf8")>7){				
				 				echo mb_substr(strip_tags($title),0,7,"utf8")."...";
							}else{
				 				echo $title;}?>
                        </a></h3>
                        <p>得票数:<span class="votenum"><?php echo $rankresult['vote'] ?></span></p>
                        <h4>姓名：<span class="playname"><?php echo strip_tags( mb_substr($rankresult['singer'],0,5,"utf8")); ?></span></h4>
                    </li>
                <?php } }?>
                <?php if($rankResultset2!=null){ foreach ($rankResultset2 as $rankresult) { $i++;?>
                    <li>
                        <span class="ranknum"><?php echo $i ?></span>
                        <a href="worksdetail.php?musicId=<?php echo $rankresult['id']?>&period=<?php echo $period;?>"><img src="<?php $pic_url=$rankresult['pic_url']; $picurl=$imgUrl->changeUrl($pic_url,"_113_64");echo $picurl; ?>"/></a>
                        <a href="worksdetail.php?musicId=<?php echo $rankresult['id']?>&period=<?php echo $period;?>"><h3><?php $title=$rankresult['title']; 
							if(mb_strlen($title,"utf8")>7){				
				 				echo mb_substr(strip_tags($title),0,7,"utf8")."...";
							}else{
				 				echo $title;}?></h3></a>
                        <p>得票数:<span class="votenum"><?php echo $rankresult['vote'] ?></span></p>
                        <h4>姓名：<span class="playname"><?php echo strip_tags( mb_substr($rankresult['singer'],0,5,"utf8")); ?></span></h4>
                    </li>
                <?php } }?>
                </ul>
                <a href="music.php" class="seemore" onClick="dataLayer.push({'event':'event','cat':'查看更多','act':'作品排行榜','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a>
            </div>
        </div>
    </div>
    <?php include_once "part/voteFloat.php"; ?>
    <?php include_once "part/footer.php"; ?>
</div>
</body>
</html>