<?php include "check.php";
 include_once "lib/data.class.php";
 $newsId="";
 if(isset($_GET['newsId'])&&!empty($_GET['newsId'])){
   $newsId=$_GET['newsId'];
 }else{
   $newsId=1;
 }
 $data=new Data();
 $result=$data->getNewsListDetailed($newsId);
 $resultset=$data->getOtherDetail($newsId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
	<meta charset="utf-8">
	<meta  name="keywords" content="<?php echo $result['title'];?>">
	<meta name="description" content="<?php echo mb_substr(strip_tags($result['content']),0,200,'utf8');?>" >
	<title>报道详情标题-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/news.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="newsDetail.php";
		pn="news";
		params=window.location.search;
		pageName+=params;
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/layer.min.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<div id="container">
  <div id="header">
    <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter"><?php include 'part/personalCenter.php'; ?></div>
    <div id="title">
      <div class="button"><?php include_once "part/navigation.php";?></div>
    </div>
  </div>  
    <div id="content" class="newsDetail">
        <div id="article">
            <article>
                <div class="newsTitle">
                    <h1>
                    <?php $title=$result['title']; 					
						 echo $title;
					?>
                    </h1>
                    <div class="time">发表时间：<?php echo $result['create_time'];?></div>
                    <hr />
                </div>
                <?php if(isset($result['picurl'])&&!empty($result['picurl'])&&empty($result['video_url'])){?>
                	<img src="<?php echo $result['picurl'];?>"  alt="image"/>
                <?php } ?>
                
                <?php if(isset($result['video_url'])&&!empty($result['video_url'])){ ?>    
                <object id="webmvplayer" name="webmvplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10.0.32" width="483" height="290">
			<param name="bgColor" value="#666666">
			<param name="movie" value="http://static.kugou.com/common/swf/video/videoPlayer.swf">
			<param name="flashvars" value="skinurl=http://static.kugou.com/common/swf/video/skin.swf&amp;aspect=true&amp;url=<?php echo $config['root'].$result['video_url']; ?>&amp;autoplay=true&amp;fullscreen=true">
			<param name="quality" value="high">
			<param name="allowScriptAccess" value="always">
			<param name="WMODE" value="transparent">
			<param name="allowFullScreen" value="true">
			<embed name="jam" src="http://static.kugou.com/common/swf/video/videoPlayer.swf" width="483" height="290" allowscriptaccess="always" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="skinurl=http://static.kugou.com/common/swf/video/skin.swf&amp;aspect=true&amp;url=<?php echo $config['root'].$result['video_url']; ?>&amp;autoplay=true&amp;fullscreen=true" type="application/x-shockwave-flash" wmode="transparent" allowfullscreen="true">
		</object>
      <?php }?> 
               
                <p><?php echo $result['content'];?></p>
            </article>
        </div>
        <div id="moreNews">
            <ul>
            <?php foreach($resultset as $res){?>
                <li>
                	<a href="newsDetail.php?newsId=<?php echo $res['id'] ?>" onclick="dataLayer.push({'event':'event','cat':'赛事报道','act':'更多新闻','lbl':'<?php echo $res['title'];?>'});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">
                    <span class="title">
					<?php $title=$res['title']; 
					if(mb_strlen($title,"utf8")>10){			
				 		echo mb_substr(strip_tags($title),0,10,"utf8")."...";
					}else{
						 echo $title;
					}?></span>
                    <span class="time"><?php echo date('Y-m-d',strtotime($res['create_time']));?></span>
               		 </a>
                </li>
            <?php } ?>    
            </ul>
            <a href="news.php" onclick="dataLayer.push({'event':'event','cat':'赛事报道','act':'更多新闻','lbl':'传递点击文字'});
setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"><div class="more"></div></a>
        </div>
    </div>
    <?php include_once "part/footer.php"; ?>
</div>
</body>
</html>
