<?php
/**
 * 酷狗登录网站接入demo
 * @author josephzeng
 * 2014-04-01
 **/
session_start();
unset($_SESSION['userOpenId']);
unset($_SESSION['userInfo']);
setcookie("userOpenId",$_SESSION['userOpenId'], time()-3600*24);
setcookie("pageName",$_SESSION['pageName'], time()-3600*24);
header("Location:index.php"); 
