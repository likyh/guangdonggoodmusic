<?php
include "check.php";
?><html>
<head>
    <meta charset="utf-8" name="keywords" content="广东好歌奖项">
	<title>【奖项设置】-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/activityIntro.css"/>
    <link rel="stylesheet" type="text/css" href="style/act.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="jiangxiang.php";
		pn="activity";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/activity.js"></script>
<script type="text/javascript" src="script/shareLink.js"></script>
	<script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
   <div id="header">
    <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter">
      <?php include 'part/personalCenter.php'; ?>
    </div>
    <div id="title">
      <div class="button"><?php include_once "part/navigation.php";?></div>
    </div>
   </div>
   <div id="content" class="activityIntroshengming1">
        <div id="share">
            分享活动到：
            <a target="_blank" class="bds_tsina" title="分享到新浪微博" href="http://v.t.sina.com.cn/share/share.php?url=http%3a%2f%2fgdhg.kugou.com%2fjiangxiang.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'新浪微博'});"><img src="style/images/sinaimg.png"/></a>
            <a target="_blank" class="bds_renren" title="分享到人人网" href="http://widget.renren.com/dialog/share?resourceUrl=http://gdhg.kugou.com/jiangxiang.php%3Futm_source%3Dsocialshare%26utm_medium%3Drenren%26utm_campaign%3D5rams%26&title=广东好歌&content=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'人人'});"><img src="style/images/renren.png"/></a>
            <a target="_blank" class="bds_douban" title="分享到豆瓣网" href="http://www.douban.com/recommend/?url=http%3a%2f%2fgdhg.kugou.com%2fjiangxiang.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'豆瓣'});"><img src="style/images/douban.png"/></a>
            <a target="_blank" class="bds_tengxun" title="分享到腾讯微博" href="http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fgdhg.kugou.com%2Fjiangxiang.php%3Futm_source%3Dsocialshare%26utm_medium%3Dtencent%26utm_campaign%3D5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'腾讯微博'});"><img src="style/images/tengxun.png"/></a>


        </div>
        <div class="navigationButton3">
            <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
        </div>
  <article>
  <!--奖品设置-->
  <img src="style/images/navline3.png"/>
  <div id="prizeSet">
     <h4>网友抽奖</h4>
    <p>1.在活动期间，购买五羊促销装甜筒，从甜筒盖下获得编码。<br>
      2.关注五羊雪糕官方微信账号，点击"码上有奖"按钮，输入五羊促销装甜筒盖下的编码，即可参与抽奖。<br>
      3.一个编码一次抽奖机会。 </p>

    <img src="style/images/prize.png"/> 
     <h6>*苹果公司非联合活动商亦非本次活动赞助商<br>
    **索尼音乐提供旗下艺人在广东省内举办的演唱会、歌友会、见面会。试用期至2015/5/21</h6>
     <br>
    <h4>参赛者</h4>
    <h4>冠军将获得：</h4>
    <p>1. 由索尼音乐娱乐主理的专业录音制作（一首单曲）
</p>
    <p>2. 由索尼音乐娱乐主理的专业制作MV（一首单曲）
</p>
    <p>3. 索尼音乐娱乐对本单曲的宣传及数字发行
</p>
<h4>总决赛选手将获得：</h4>
<p>与明星评审、音乐评审最后直播演出的表演机会</p>
</div>
  </article>
    </div>

</div>
<?php include_once "part/footer.php"; ?>
<!--END #container-->
</body>
</html>
