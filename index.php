<?php
  include "check.php";
  include_once "lib/data.class.php";
  include_once "lib/news.class.php";
  $data=new Data();
  $notice=$data->getNotice();
  $resultset=$data->getNewsListRough();
  $period=$data->getPeriod();
 $gr=new GameReport();
 $pagesize=3;
$period=$data->getPeriod();
 $rows=$gr->getWordsRankRowsByPeriod();//一共有多少条记录
 if($rows>0){
    $pages=$gr->getPages($rows,$pagesize);//一共有多少页
    $currentpage=isset($_GET['page'])?(int)$_GET['page']:1;
 	if($currentpage<1){
  	  $currentpage=1;
 	}else if($currentpage>$pages){
 	  $currentpage=$pages;
 	}
 	$prevPage=$currentpage-1>0?$currentpage-1:1;
 	$nextPage=$currentpage+1<=$pages?$currentpage+1:$pages;

 	// $pageresultset=$data->finalWorkPaging($currentpage,$pagesize);
  $finalVote=$data->finalVote();
   }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta  name="keywords" content="广东好歌，广东好歌官网，广东好歌选秀大赛广东好歌报名网站">
<meta name="description" content="广东好歌是由雀巢五羊雪糕倾心打造的大型粤语歌曲选秀大赛，3月19日开始海选报名，了解更多请访问网络报名唯一指定网站。" >
<title>广东好歌选秀大赛官方网站，网络报名唯一指定网站</title>
<link href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="style/common.css"/>
<!--<link rel="stylesheet" type="text/css" href="style/styles.css"/>-->
<link rel="stylesheet" type="text/css" href="style/works_rank.css"/>
<script type="text/javascript" src="script/reset.js"></script>
<script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="script/layer.min.js"></script>
<script type="text/javascript" src="script/getCookie.js"></script>
<script type="text/javascript">
    pageName="index.php";
	pn="index";
	if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
    dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
</script>
<script type="text/javascript" src="script/vote.js"></script>
<script type="text/javascript" src="script/shareLink.js"></script>
<script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container"> 
  <!--头部-->
  <div id="header">
    <div id="personalCenter">
      <?php include 'part/personalCenter.php'; ?>
    </div>
    <div id="companyAndScroll"> <img src="style/images/chengbandanwei.png"/>
      <marquee behavior="scroll" direction="left" scrollamount="2">
      <?php echo $notice;?>
      </marquee>
    </div>
    <div id="title">
      <div class="button">
          <?php include_once "part/navigation.php";?>
      </div>
    </div>
  </div>
  <!--End header--> 
  
  <!--BEGIN main-->
  <div id="main"> 
    <div id="lottery"><a href="lotteryEnd.php" onclick="dataLayer.push({'event':'event','cat':'投票成功','act':'点击抽奖','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;">立刻抽奖</a></div>
    <div id="bottomSide">
    <div id="words_rank"> <a href="music.php" class="more" onclick="dataLayer.push({'event':'event','cat':'查看更多','act':'作品排行榜','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a>
      <div id="works">
        <!-- -->
        <div id="work_item">
          <div class="work_img">
          <a href="worksdetail.php?musicId=2453311&period=6"><img src="http://gdhg.kugou.com/pic/work/liyouchen.jpg" alt="长堤派对 李佑晨" /><img src="style/images/videoplaybtn.png" id="playLogo"/></a></div>
          <div class="profile">
            <div class="title"><a href="worksdetail.php?musicId=2453311&period=6">长堤派对
            </a></div>
            <div class="name">李佑晨</div>
            <div class="name">冠军、最具人气奖</div>
          </div>
        </div>
        <div id="work_item">
          <div class="work_img">
          <a href="worksdetail.php?musicId=2452376&period=6"><img src="http://gdhg.kugou.com/pic/work/yexiuduo.jpg" alt="绿色的家园 叶秀朵" /><img src="style/images/videoplaybtn.png" id="playLogo"/></a></div>
          <div class="profile">
            <div class="title"><a href="worksdetail.php?musicId=2452376&period=6">绿色的家园
            </a></div>
            <div class="name">叶秀朵</div>
            <div class="name">最具正能量奖</div>
          </div>
        </div>
        <div id="work_item">
          <div class="work_img">
          <a href="worksdetail.php?musicId=2454294&period=6"><img src="http://gdhg.kugou.com/pic/work/hongchen.jpg" alt="氹氹转 洪尘" /><img src="style/images/videoplaybtn.png" id="playLogo"/></a></div>
          <div class="profile">
            <div class="title"><a href="worksdetail.php?musicId=2454294&period=6">氹氹转
            </a></div>
            <div class="name">洪尘</div>
            <div class="name">最具广东风范奖</div>
          </div>
        </div>
        <!-- -->
      </div>
    </div>

      <div id="news_report">
        <div id="news_report_images"><img src="style/images/newspic .jpg" alt="广东好歌选秀大赛" /></div>
        <div class="news">
            <?php foreach($resultset as $res){?>
            <a href="newsDetail.php?newsId=<?php echo $res['id']; ?>"><p><?php echo mb_substr(strip_tags($res['title']),0,12,'utf8')."...";?></p></a>
            <?php }?>
            <a href="news.php" class="more" onclick="dataLayer.push({'event':'event','cat':'查看更多','act':'赛事报道','lbl':''});setTimeout('window.location=&quot;'+this.href+'&quot;',500);return false;"></a>
        </div>
      </div>
    </div>
  </div>
  <!--End main-->

    <?php include_once "part/voteFloat.php"; ?>
    <?php include_once "part/footer.php"; ?>
  
</div>
</body>
</html>
