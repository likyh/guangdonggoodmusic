<?php
include 'check.php';
include_once "../lib/music.class.php";
if(isset($_POST)){
    if(isset($_POST['update1'])){
        $api="http://hd.5sing.com/gdhg/jsondata.html?pagesize=10000";
        $period=1;
    } 
    if(isset($_POST['update2'])){
        $api="http://gdhg.5sing.com/gdhg/jsondata2.html?pagesize=10000";
        $period=2;
    }
    if(isset($_POST['update3'])){
        $api="http://hd.5sing.com/gdhg/getDataTop25.html?pagesize=10000";
        $period=3;
    }
    if(isset($_POST['update4'])){
        $api="repechage.html";
        $period=4;
    }
    if(isset($_POST['update5'])){
        $api="semifinal.html";
        $period=5;
    }
    $music=new Music();
    if($music->refresh($api,$period)){
        $result="更新成功";
    }else{
        $result="更新失败";
    }    
}else{
    $result="更新失败";
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="musicUpdate.php">更新作品信息</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
        <?php
            echo $result;
        ?>
       </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>