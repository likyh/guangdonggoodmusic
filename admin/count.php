<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$freeVote=$db->getValue("SELECT count(1) from vote_history where `unique_code` IS NULL");
$codeVote=$db->getValue("SELECT count(1) from vote_history where `unique_code` IS NOT NULL");
$totalPeople=$db->getValue("SELECT count(distinct open_id) from vote_history");
$totalVote=(int)$freeVote+(int)$codeVote;

$pcSql="SELECT count(*) as `pc_sum`,`date` FROM `vote_history` where `date`>=172 and `terminal`=1 group by `date`";
$result['pc']=$db->getAll($pcSql);
$mSql="SELECT count(*) as `m_sum`,`date` FROM `vote_history` where `date`>=172 and `terminal`=0 group by `date`";
$result['m']=$db->getAll($mSql);
foreach ($result['pc'] as $key1 => $pc) {
    foreach ($result['m'] as $key2 => $m) {
        if($pc['date']==$m['date']){
            $result['pc'][$key1]['m_sum']=$m['m_sum'];
        }
    }
}
$beginDay= '2014-06-21';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="count.php">统计信息</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th width="25%">免费投票次数</th>
                    <th width="25%">编码投票次数</th>
                    <th width="25%">总投票次数</th>
                    <th width="25%">总投票人数</th>
                </tr>
                </thead>
                <tbody>

    <tr>
        <td><?php echo $freeVote ?></td>
        <td><?php echo $codeVote?></td>
        <td><?php echo $totalVote ?></td>
        <td><?php echo $totalPeople ?></td>
    </tr>
                </tbody>
            </table>
            <br/>
            <!-- add-->
            <table id="dataTable" >
                <thead>
                <tr>
                    <th width="25%">日 期</th>
                    <th width="25%">PC端</th>
                    <th width="25%">mobile端</th>
                    <th width="25%">天总票数</th>
                </tr>
                </thead>
                <tbody>
                     <?php foreach ($result['pc'] as $key => $value) {?>
                <tr>
                    <td><?php  $add=$value['date']-172; echo date('Y-m-d',strtotime("$beginDay +$add day")); ?></td>
                    <td><?php echo $value['pc_sum']*5?></td>
                    <td><?php $m_sum=isset($value['m_sum'])? $value['m_sum']:0; echo $m_sum*5?></td>
                    <td><?php echo ($m_sum+$value['pc_sum'])*5?></td>
                </tr><?php }?> 
                </tbody>
            </table>
            <br/>
           
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            