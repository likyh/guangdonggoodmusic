<?php
include 'check.php';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        function Textcheck(){
            var content=document.getElementById('contentInput').value.trim();
            if(content==''){
                alert('请认真填好公告栏内容');
                return false;
            }

        }
    </script>   
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="noticeManager.php">公告栏管理</a>&gt;<a href="noticeAdd.php">新建公告</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<form class="form_style" enctype="multipart/form-data" action="noticeAddAction.php" method="post">
    <fieldset>
        <legend>  </legend>
        <input type="text" name="content" id="contentInput" placeholder="请输入新的公告栏内容">
    </fieldset>
    <input id="button" onclick="return Textcheck();" type="submit" value="提 交">
</form>
    </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>