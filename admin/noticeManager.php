<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$sql="SELECT `id`,`content`,`create_time` FROM `notice` ORDER BY `create_time` DESC limit 10";
$result=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="noticeManager.php">公告栏管理</a></span></div>
            <div id="contentControl">
                <a href="noticeAdd.php"><div class="button" id="contentBtnAdd"></div></a>
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th>id</th>
                    <th>内容</th>
                    <th>时间</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>&nbsp;</th>
                    <th>[只显示最新的10条]</th>
                    <th>&nbsp;</th>
                </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($result as $q){ ?>
    <tr>
        <td><?php echo $q['id'] ?></td>
        <td><?php echo $q['content']?></td>
        <td><?php echo $q['create_time']?></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            