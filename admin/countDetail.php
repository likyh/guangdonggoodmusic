<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
//
$getAllcount="SELECT count(*) as `tmp`  FROM  `vote_history`, `music`
where `music_id` in ('2408173','2433561','1631691','2452375','2402507','2453310','2385683','2442137','2454293','2425601','2399954','2450526')
and `music`.`id`=`vote_history`.`music_id`
and `date`>=172
 group by `music_id`,`date`";
$getAllcountR=$db->getAll($getAllcount);
$total=0;
foreach ($getAllcountR as $key => $value) {
    $total++;
}
if($total%15==0){
    $pageNum=$total/15;
}else{
    $pageNum=($total/15)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*15;
//
$sql="SELECT count(*) as `one_sum`,`title`,`date`,`singer` FROM `vote_history`,`music`
where `music_id` in ('2408173','2433561','1631691','2452375','2402507','2453310','2385683','2442137','2454293','2425601','2399954','2450526')
and `music`.`id`=`vote_history`.`music_id`
and `date`>=172
group by `music_id`,`date` order by `date` desc limit $start,15;";
 $result['detail']=$db->getAll($sql);
$beginDay= '2014-06-21';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="count.php">统计信息</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th width="25%">歌名</th>
                    <th width="25%">选手</th>
                    <th width="25%">日 期</th>
                    <th width="25%">天得票数</th>
                
                </tr>
                </thead>
                <tbody><?php foreach ($result['detail'] as $key => $value) {
                  ?>
                    <tr>
                        <td><?php echo $value['title'] ?></td>
                        <td><?php echo $value['singer'] ?></td>
                        <td><?php $add=$value['date']-172; echo date('Y-m-d',strtotime("$beginDay +$add day"));?></td>
                        <td><?php echo $value['one_sum']*5 ?></td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $total; ?></span>条记录</li>
                    <?php if(($page-1)>0){?>
                    <li ><a href="countDetail.php?page_id=<?php echo $page-1; ?>">上一页</a></li>
                    <?php }?>
                        <?php for($i=1;$i<=$pageNum;$i++){?>
                        <li class="page"><a href="countDetail.php?page_id=<?php echo $i ?>"><?php if($page==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                        <?php } ?>
                        <?php if(($page+1)<$pageNum){?>
                    <li ><a href="countDetail.php?page_id=<?php echo $page+1; ?>">下一页</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            