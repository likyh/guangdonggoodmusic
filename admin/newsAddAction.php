<?php
include 'check.php';
include 'Uploader.class.php';
include_once "../lib/SqlDB.class.php";
$db= SqlDB::init();
if(empty($_POST)){
    $videoMessage='视频文件太大';
    $imgMessage='图片上传失败';
}
if(isset($_POST['submit'])){//isset($_POST['title'])&&isset($_POST['content'])
    $title=$db->quote($_POST['title']);
    $content=$db->quote($_POST['content']);
if(isset($_FILES['picurl'])&&!empty($_FILES['picurl'])){
   // 处理图片上传
    $imgSavePathConfig ='img';
    if ( !file_exists( $imgSavePathConfig ) ) 
   mkdir( $imgSavePathConfig , 0777 , true ); 
    $config = array(
        "savePath" => $imgSavePathConfig,
        "maxSize" => 5000, //单位KB
        "allowFiles" => array(".gif", ".png", ".jpg", ".jpeg", ".bmp"),
        "fileNameFormat" => time()
    );
    $config[ 'savePath' ] = $config[ 'savePath' ] . '/';
    $up=new Uploader('picurl',$config);
    $info = $up->getFileInfo();
    if($info['state']=='SUCCESS'){
        $imgMessage='图片上传成功';
        $newurl=$info['url'];
        $dbUrl='admin/'.$newurl;
    }else{
        $imgMessage='图片上传失败';
        $dbUrl=null;
    }
}else{
    $dbUrl=NULL;
    $imgMessage='未上传图片';
}
if(isset($_FILES['videourl'])&&!empty($_FILES['videourl'])){
   //处理视频上传
    $videoSavePathConfig ='video';
    if ( !file_exists( $videoSavePathConfig ) ) 
   mkdir( $videoSavePathConfig , 0777 , true ); 
    $v_config = array(
        "savePath" => $videoSavePathConfig,
        "maxSize" => 90000, //单位KB
        "allowFiles" => array(".3gp", ".avi", ".mp4", ".flv"),
        "fileNameFormat" => time()
    );
    $v_config[ 'savePath' ] = $v_config[ 'savePath' ]. '/';
    $v_up=new Uploader('videourl',$v_config);
    $v_info = $v_up->getFileInfo();
    if($v_info['state']=='SUCCESS'){
        $videoMessage='视频上传成功';
        $v_newurl=$v_info['url'];
        $v_dbUrl='admin/'.$v_newurl;
    }else{
        $videoMessage='视频上传失败';
        $v_dbUrl=null;
    }
}else{
    $v_dbUrl=NULL;
    $videoMessage='未上传视频';
}
 

    $sql="INSERT INTO `news` (`title`,`content`,`picurl`,`video_url`)VALUES ({$title},{$content},'{$dbUrl}','{$v_dbUrl}')";
    if($db->sqlExec($sql)>0){
        $textMessage="内容修改成功";
    }else{
        $textMessage="内容修改失败";
    }
}else{
    $textMessage="内容修改失败";
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
        <?php
            echo $textMessage.' ['.$imgMessage.'] '.' ['.$videoMessage.'] '.'<a href="newsManager.php">返回</a>';
        ?>
       </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>