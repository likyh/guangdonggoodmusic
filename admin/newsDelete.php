<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$id=(int)$_GET['id'];
$sql="DELETE FROM `news`
WHERE `id`={$id};";
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<?php
if($db->sqlExec($sql)>0){
    echo "删除成功<a href='newsManager.php'>返回</a>";
    header("location:newsManager.php");
}else{
    echo "删除失败<a href='newsManager.php'>返回</a>";
}?>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>