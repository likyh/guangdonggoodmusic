<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db= SqlDB::init();
if(isset($_POST['periodInput'])){
    $periodInput=$db->quote($_POST['periodInput']);
    try{
        $sql='update `cur_period` set `period`='.$periodInput.'where `id`=1';
        $db->sqlExec($sql);
    }catch(PDOException $e){
    }
    $period=$_POST['periodInput'];
}else{
    $sql="select `period` from `cur_period` where `id`=1";
    $period=$db->getValue($sql);
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="periodManager.php">比赛阶段</a>&gt;</span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<form class="form_style" action="periodManager.php" method="post">
    <fieldset>
        <legend>  </legend>
        <label for ="periodInput">比赛阶段</label>
        <select name="periodInput" id="periodInput">
            <option value="1" <?php if($period=='1') echo 'selected' ?>>海选</option>
            <option value="2" <?php if($period=='2') echo 'selected' ?>>50进25</option>
            <option value="3" <?php if($period=='3') echo 'selected' ?>>25进10</option>
            <option value="4" <?php if($period=='4') echo 'selected' ?>>复活赛</option>
            <option value="5" <?php if($period=='5') echo 'selected' ?>>12进决赛</option>
            <option value="6" <?php if($period=='6') echo 'selected' ?>>总决赛</option>
        </select>
    </fieldset>
    <input id="button" type="submit" >
</form>
    </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>