<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$id=(int)$_GET['id'];
$sql="SELECT * FROM `music` where `id`={$id};";
$music=$db->getOne($sql);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        function Textcheck(){
            var content=document.getElementById('contentInput').value.trim();
            if(content==''){
                alert('请认真填好作品内容');
                return false;
            }

        }
    </script>   
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="musicManager.php">作品管理</a>&gt;<a href="musicModify.php">作品修改</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<form class="form_style" enctype="multipart/form-data" action="musicModifyAction.php" method="post">
    <fieldset>
        <legend>  </legend>
        <input type="hidden" name="id" value="<?php echo $music['id'] ?>">
        <span>视频URL：&nbsp</span><input type="text" name="video_url" value="<?php echo $music['video_url'] ?>" id="fx_urlInput" placeholder="请输入视频URL"><br/>
        <span>音乐URL：&nbsp</span><input type="text" name="music_url" value="<?php echo $music['music_url'] ?>" id="fx_urlInput" placeholder="请输入音乐URL"><br/>
        <span>选手照片：</span><input type="text" name="photo" value="<?php echo $music['photo'] ?>" id="fx_urlInput" placeholder="请输入选手照片URL"><br/>
        <span>视频缩略图</span><input type="text" name="pic_url" value="<?php echo $music['pic_url'] ?>" id="fx_urlInput" placeholder="请输入视频略缩图URL"><br/>
        <span>作品名：&nbsp&nbsp&nbsp</span><input type="text" name="title" value="<?php echo $music['title'] ?>" id="titleInput" placeholder="请输入作品名"><br/>
        <span>选手名：&nbsp&nbsp&nbsp</span><input type="text" name="singer" value="<?php echo $music['singer'] ?>" id="singerInput" placeholder="请输入选手名"><br/>
        <span>平均分：&nbsp&nbsp&nbsp</span><input type="text" name="score" value="<?php echo $music['score'] ?>" id="num" placeholder="请输入平均分"><br/>
        <span>票数：&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><input type="text" name="vote" value="<?php echo $music['vote'] ?>" id="num" placeholder="请输入票数"><br/>
        <span>灵感：&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><input type="text" name="inspiration" value="<?php echo $music['inspiration'] ?>" id="fx_urlInput" placeholder="请输入作品灵感"><br/>
        <span>个人简介：</span><input type="text" name="profile" value="<?php echo $music['profile'] ?>" id="fx_urlInput" placeholder="请输入个人简介"><br/>
        <span>自我介绍：</span><input type="text" name="introduction" value="<?php echo $music['introduction'] ?>" id="fx_urlInput" placeholder="请输入自我介绍"><br/>

    </fieldset>
    <input id="button" onclick="return Textcheck();" type="submit" value="提 交">
</form>
    </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>