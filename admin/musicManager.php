<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$period=$db->getValue("SELECT `period` from `cur_period` WHERE `id`=1");
$result=$db->getOne("SELECT count(*) as total from `music` WHERE `period`=$period");
if($result['total']%15==0){
    $pageNum=$result['total']/15;
}else{
    $pageNum=($result['total']/15)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*15;
$sql="SELECT `id`, `title`, `singer`, `vote`,`score`,`video_url`,`email`,`tel` FROM `music` WHERE `period`=$period ORDER BY `vote` DESC limit $start,15;";
$musicArray=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script type="text/javascript">
    function makeSureDelete(){
        if(confirm('确定要删除吗   删除就无法恢复了')){
            return true;
        }else{
            return false;
        }
    }
    </script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="musicManager.php">作品管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th width="8%">id</th>
                    <th width="20%">作品名（当前阶段）</th>
                    <th width="8%">选手</th>
                    <th width="13%">Email</th>
                    <th width="12%">Tel</th>
                    <th width="7%">票数</th>
                    <th width="7">评分</th>
                    <th width="17%">视频URL</th>
                    <th width="4%">编辑</th>
                    <th width="4%">删除</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th width="8%">id</th>
                    <th width="20%">作品名（当前阶段）</th>
                    <th width="8%">选手</th>
                    <th width="13%">Email</th>
                    <th width="12%">Tel</th>
                    <th width="7%">票数</th>
                    <th width="7">评分</th>
                    <th width="17%">视频URL</th>
                    <th width="4%">编辑</th>
                    <th width="4%">删除</th>
                </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($musicArray as $q){ ?>
    <tr>
        <td><?php echo $q['id'] ?></td>
        <td><?php echo $q['title']?></td>
        <td><?php echo $q['singer']?></td>
        <td><?php echo $q['email']?></td>
        <td><?php echo $q['tel']?></td>
        <td><?php echo $q['vote']?></td>
        <td><?php echo $q['score']?></td>
        <td><?php echo $q['video_url']?></td>
        <td><a href="musicModify.php?id=<?php echo $q['id'] ?>">编辑</a></td>
        <td><a onclick="return makeSureDelete();" href="musicDelete.php?id=<?php echo $q['id'] ?>">删除</a></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
                    <?php if(($page-1)>0){?>
                    <li ><a href="musicManager.php?page_id=<?php echo $page-1; ?>">上一页</a></li>
                    <?php }?>
                        <?php for($i=1;$i<=$pageNum;$i++){?>
                        <li class="page"><a href="musicManager.php?page_id=<?php echo $i ?>"><?php if($page==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                        <?php } ?>
                        <?php if(($page+1)<$pageNum){?>
                    <li ><a href="musicManager.php?page_id=<?php echo $page+1; ?>">下一页</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            