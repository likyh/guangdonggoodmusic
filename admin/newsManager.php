<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$result=$db->getOne("select count(*) as total from `news` where 1");
if($result['total']%11==0){
    $pageNum=$result['total']/11;
}else{
    $pageNum=($result['total']/11)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*11;
$sql="SELECT `id`, `title`, `create_time` FROM `news` ORDER BY `create_time` DESC limit $start,11;";
$newsArray=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script type="text/javascript">
    function makeSureDelete(){
        if(confirm('确定要删除吗')){
            return true;
        }else{
            return false;
        }
    }
    </script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
            <div id="contentControl">
                <a href="newsAdd.php"><div class="button" id="contentBtnAdd"></div></a>
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th>id</th>
                    <th>标题</th>
                    <th>时间</th>
                    <th>编辑</th>
                    <th>删除</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>id</th>
                    <th>标题</th>
                    <th>时间</th>
                    <th>编辑</th>
                    <th>删除</th>
                </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($newsArray as $q){ ?>
    <tr>
        <td><?php echo $q['id'] ?></td>
        <td><?php echo $q['title']?></td>
        <td><?php echo $q['create_time']?></td>
        <td><a href="newsModify.php?id=<?php echo $q['id'] ?>">编辑</a></td>
        <td><a onclick="return makeSureDelete();" href="newsDelete.php?id=<?php echo $q['id'] ?>">删除</a></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
                    <?php if(($page-1)>0){?>
                    <li ><a href="newsManager.php?page_id=<?php echo $page-1; ?>">上一页</a></li>
                    <?php }?>
                        <?php for($i=1;$i<=$pageNum;$i++){ if(($page-10<=$i)&&($i<=$page+10)){?>
                        <li class="page"><a href="newsManager.php?page_id=<?php echo $i ?>"><?php if($page==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                        <?php } }?>
                        <?php if(($page+1)<$pageNum){?>
                    <li ><a href="newsManager.php?page_id=<?php echo $page+1; ?>">下一页</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            