    <nav>
        <h2>Menu</h2>
        <div class="menuItem index">
            <div class="title"><a href="index.php"><img src="style/images/nav_home.png">首 页 &nbsp;&gt;</a></div>
        </div>
        <div class="menuItem">
            <div class="title"><a href="newsManager.php"><img src="style/images/nav_text.png">文章管理 &nbsp;&gt;</a></div>
        </div>
        <div class="menuItem">
            <div class="title"><a href="musicManager.php"><img src="style/images/nav_text.png">作品管理 &nbsp;&gt;</a></div>
        </div>        
        <div class="menuItem">
            <div class="title"><a href="periodManager.php"><img src="style/images/nav_text.png">比赛阶段 &nbsp;&gt;</a></div>
        </div>
         <div class="menuItem">
            <div class="title"><a href="noticeManager.php"><img src="style/images/nav_text.png">公告栏管理 &nbsp;&gt;</a></div>
        </div>       
        <div class="menuItem">
            <div class="title"><a href="musicUpdate.php"><img src="style/images/nav_text.png">更新作品信息 &nbsp;&gt;</a></div>
        </div>
        <div class="menuItem">
            <div class="title"><a href="messageManager.php"><img src="style/images/nav_text.png">评论管理 &nbsp;&gt;</a></div>
        </div>        
        <div class="menuItem">
            <div class="title"><a href="count.php"><img src="style/images/nav_text.png">总投票统计 &nbsp;&gt;</a></div>
        </div>
        <div class="menuItem">
            <div class="title"><a href="countDetail.php"><img src="style/images/nav_text.png">详细票数统计 &nbsp;&gt;</a></div>
        </div>       
    </nav>