<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db= SqlDB::init();
$id=(int)$_POST['id'];
if(isset($_POST['id'])&&isset($_POST['title'])&&isset($_POST['singer'])){
$title=$db->quote($_POST['title']);
$singer=$db->quote($_POST['singer']);
$video_url=$db->quote($_POST['video_url']);
$music_url=$db->quote($_POST['music_url']);
$pic_url=$db->quote($_POST['pic_url']);
$photo=$db->quote($_POST['photo']);
$score=$db->quote($_POST['score']);
$vote=$db->quote((int)$_POST['vote']);
$inspiration=$db->quote($_POST['inspiration']);
$profile=$db->quote($_POST['profile']);
$introduction=$db->quote($_POST['introduction']);
$sql="UPDATE `music` SET `title`=$title,`singer`=$singer,`video_url`=$video_url,`music_url`=$music_url,
`score`=$score,`vote`=$vote,`photo`=$photo,`pic_url`=$pic_url,`inspiration`=$inspiration,`profile`=$profile,`introduction`=$introduction
where `id`=$id";
$result=$db->sqlExec($sql);
if($result){
    $textMessage="作品修改成功";
}else{
    $textMessage="作品修改失败";
}}
else{
    $textMessage="作品修改失败";
}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="musicManager.php">作品管理</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
        <?php
            echo $textMessage.'<a href="musicManager.php">返回</a>';
        ?>
       </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>