<?php
include 'check.php';
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="musicUpdate.php">更新作品信息</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<form class="form_style" action="musicUpdateAction.php" method="post">
    <fieldset>
        <legend>  </legend>
        <label for ="periodInput"></label><span>（更新若慢，请稍等……）</span>
    </fieldset>
    <input id="button" type="submit" value="更新海选" name="update1"><br/>
    <input id="button" type="submit" value="更新50强" name="update2"><br/>
    <input id="button" type="submit" value="更新25强" name="update3"><br/>
    <input id="button" type="submit" value="更新至复活赛" name="update4"><br/>
    <input id="button" type="submit" value="更新至12进决" name="update5">
</form>
    </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>