<?php
include 'check.php';
?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <link href="style/form.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        $(document).ready(function () {
            UE.getEditor('contentInput');
        });
        function Textcheck(){
            var title=document.getElementById('titleInput').value.trim();
            var content=document.getElementById('contentInput').value.trim();
            if(title==''||content==''){
                alert('请认真填好新闻标题和内容');
                return false;
            }

        }
    </script>   
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a>&gt;<a href="newsAdd.php">新建文章</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
<form class="form_style" enctype="multipart/form-data" action="newsAddAction.php" method="post">
    <fieldset>
        <legend>  </legend>
        <input type="text" name="title" id="titleInput" placeholder="请输入标题">
        <span>[默认当前时间]</span><br/>
        <label for="picurl"><span>[首页图片:（图片请勿超过2M）,建议比例约width:200px; height:120px，不要使用竖向图片作为首页图片，会被压缩]<span></label>
        <input type="file" id="picurl" name="picurl" /><br/>
        <label for="videourl"><span>[视频:（视频请勿超过150M，否则会上传失败）]<span></label>
        <input type="file" id="videourl" name="videourl" /><br/>
        <label for="content"><span>[正文部分：从其他地方拷贝过来的文字请先使用工具栏左边的（橡皮擦）清除格式，否则前台显示异常]<span></label>
        <textarea name="content" id="contentInput" placeholder="请输入内容"></textarea>
    </fieldset>
    <input id="button" onclick="return Textcheck();" type="submit" value="提 交" name="submit">
</form>
    </div>
</div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>