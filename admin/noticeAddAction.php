<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db= SqlDB::init();
if(isset($_POST['content'])){
$content=$db->quote($_POST['content']);
$sql="INSERT INTO `notice` (`content`)values($content)";
$result=$db->sqlExec($sql);
if($result){
    $textMessage="公告栏更新成功";
}else{
    $textMessage="公告栏更新失败";
}}
else{
    $textMessage="公告栏更新失败";
}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="noticeManager.php">公告栏管理</a></span></div>
         <div id="contentControl">
                
        </div>
        <div id="data">
        <?php
            echo $textMessage.'<a href="noticeManager.php">返回</a>';
        ?>
       </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>