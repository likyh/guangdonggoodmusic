<?php
include 'check.php';
include_once "../lib/SqlDB.class.php";
$db=SqlDB::init();
$result=$db->getOne("SELECT count(*) as total from `comment` ");
if($result['total']%13==0){
    $pageNum=$result['total']/13;
}else{
    $pageNum=($result['total']/13)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*13;
$sql="SELECT `id`,`music_id`,`username`, `content`, `timestamp`  FROM `comment` order by `timestamp` DESC limit $start,13;";
$messageArray=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>GDMusicCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script type="text/javascript">
    function makeSureDelete(){
        if(confirm('确定要删除吗   删除就无法恢复了')){
            return true;
        }else{
            return false;
        }
    }
    </script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="index.php">后台首页</a>&gt;<a href="messageManager.php">评论管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th width="8%">作品id</th>
                    <th width="15%">用户</th>
                    <th width="55%">内容</th>
                    <th width="16">时间</th>
                    <th width="6%">删除</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th width="8%">作品id</th>
                    <th width="15%">用户</th>
                    <th width="55%">内容</th>
                    <th width="16">时间</th>
                    <th width="6%">删除</th>
                </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($messageArray as $q){ ?>
    <tr>
        <td><?php echo $q['music_id']?></td>
        <td><?php echo $q['username']?></td>
        <td><?php echo $q['content']?></td>
        <td><?php echo $q['timestamp']?></td>
        <td><a onclick="return makeSureDelete();" href="messageDelete.php?id=<?php echo $q['id'] ?>">删除</a></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
                    <?php if(($page-1)>0){?>
                    <li ><a href="messageManager.php?page_id=<?php echo $page-1; ?>">上一页</a></li>
                    <?php }?>
                        <?php for($i=1;$i<=$pageNum;$i++){ if(($page-10<=$i)&&($i<=$page+10)){?>
                        <li class="page"><a href="messageManager.php?page_id=<?php echo $i ?>"><?php if($page==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                        <?php } }?>
                        <?php if(($page+1)<$pageNum){?>
                    <li ><a href="messageManager.php?page_id=<?php echo $page+1; ?>">下一页</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php include "part/footer.php"; ?>  
</div>
</body>
</html>            