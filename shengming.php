<?php
include "check.php";
?><html>
<head>
    <meta charset="utf-8" name="keywords" content="广东好歌活动声明">
	<title>【活动声明】-广东好歌选秀大赛官方网站</title>
    <link href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style/common.css"/>
    <link rel="stylesheet" type="text/css" href="style/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/activityIntro.css"/>
    <link rel="stylesheet" type="text/css" href="style/act.css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="style/ieFix.css"/>
    <![endif]-->
    <script type="text/javascript" src="script/reset.js"></script>
    <script type="text/javascript" src="script/getCookie.js"></script>
    <script type="text/javascript">
        pageName="shengming.php";
		pn="activity";
		if(getCookie('userOpenId')!=""){
	 userId=getCookie('userOpenId');
	}else{
	 userId="";
	}
        dataLayer.push({'event':'page','branch':'/','section':pageName,'pname':'','userid':userId});
    </script>
    <script type="text/javascript" src="script/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="script/activity.js"></script>
    <script type="text/javascript" src="script/shareLink.js"></script>
    <script type="text/javascript" src="script/highlight.js"></script>
</head>

<body>
<!--BEGIN #container-->
<div id="container">
   <div id="header">
     <div id="company">承办单位：<img src="style/images/sonymusic.png"/><img src="style/images/kugou.png"/></div>
    <div id="personalCenter">
      <?php include 'part/personalCenter.php'; ?>
    </div>
    <div id="title">
      <div class="button"><?php include_once "part/navigation.php";?></div>
    </div>
  </div>
    <div id="content" class="activityIntro">
        <div id="share">
            分享活动到：
          <a target="_blank" class="bds_tsina" title="分享到新浪微博" href="http://v.t.sina.com.cn/share/share.php?url=http%3a%2f%2fgdhg.kugou.com%2fshengming.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'新浪微博'});"><img src="style/images/sinaimg.png"/></a>
            <a target="_blank" class="bds_renren" title="分享到人人网" href="http://widget.renren.com/dialog/share?resourceUrl=http://gdhg.kugou.com/shuoming.php%3Futm_source%3Dsocialshare%26utm_medium%3Drenren%26utm_campaign%3D5rams%26&title=广东好歌&content=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'人人'});"><img src="style/images/renren.png"/></a>
            <a target="_blank" class="bds_douban" title="分享到豆瓣网" href="http://www.douban.com/recommend/?url=http%3a%2f%2fgdhg.kugou.com%2fshengming.php%3futm_source%3dsocialshare%26utm_medium%3dsina%26utm_campaign%3d5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'豆瓣'});"><img src="style/images/douban.png"/></a>
            <a target="_blank" class="bds_tengxun" title="分享到腾讯微博" href="http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fgdhg.kugou.com%2Fshengming.php%3Futm_source%3Dsocialshare%26utm_medium%3Dtencent%26utm_campaign%3D5rams&title=%23%E5%B9%BF%E4%B8%9C%E5%A5%BD%E6%AD%8C%2350%E5%BC%BA%E7%AA%81%E5%9B%B4%E8%AF%9E%E7%94%9F%EF%BC%8C%E5%BF%AB%E6%9D%A5%E4%B8%BA%E4%BD%A0%E5%BF%83%E4%B8%AD%E7%9A%84%E5%A5%BD%E6%AD%8C%E6%8A%95%E7%A5%A8%EF%BC%8C%E8%BF%98%E6%9C%89%E7%A5%9E%E7%A7%98%E5%A5%BD%E7%A4%BC%E7%AD%89%E4%BD%A0%E5%93%A6%21" onClick="dataLayer.push({'event':'event','cat':'活动详情','act':'分享','lbl':'腾讯微博'});"><img src="style/images/tengxun.png"/></a>
        </div>
        <div class="navigationButton4">
            <a href="shuoming.php" class="act">活动说明</a>
            <a href="pingwei.php" class="jud">评委构成</a>
            <a href="jiangxiang.php" class="pri">奖项设置</a>
            <a href="shengming.php" class="stat">活动声明</a>
        </div>
  <article>
  <!--活动声明-->
  <img src="style/images/navline4.png"/>
  <div id="actStat">
   <p>1. 严格遵守原创要求，参赛者要保证其参赛作品为原创作品，保证拥有该原创作品的完整版权，并确保该作品不曾被用于任何商业用途，未转让或许可给他人。因作品侵权造成的一切法律后果由选手自行承担。</p>
    <p>2. 参赛者允许主办方及承办方在与比赛相关的活动中发布和免费使用参赛作品、选手姓名和其他必要信息。</p>
    <p>3.参赛者一旦填报信息和提交参赛作品，将视为充分理解和同意本赛制，并接受赛制约束。</p>
    <p>4.参赛作品不得违反中华人民共和国法律法规；不得含有反对国家或政府（包括其他国家或地区）的内容；不得含有种族宗教及身份<br>歧视、诬蔑民族传统文化、泄露国家或商业机密、侵犯他人隐私、侮辱或诽谤他人人格、歧视残疾人等内容。</p>
    <p>5. 所有奖品图片仅供参考，奖品均以收到实物为准。</p>
    <p>6. 本次活动主办方及其承办方有权使用全部参赛选手的参赛作品、肖像及参赛视频等资料用于五羊雪糕"广东好歌"的海报、网<br>络、电台等媒介的宣传和传播中，传播地区为中国大陆，传播期限为活动启动之日起至2014年8月31日活动结束。</p>
    <p>7. 本次活动主办方及其承办方有权使用25强的参赛作品、肖像及参赛视频等资料用于主办方的品牌宣传活动，使用地区为中国<br>大陆，使用期限为活动启动之日起至2014年12月31日。其中，经由索尼音乐娱乐制作的冠军单曲及相关联音乐视频可用于主办方的品牌宣传活动，使用地区为中国大陆，使用期限为自首次发行之日起两年。</p>
    <p>8.决赛中进行的录音、录像、摄像和照片等资料的有关版权归主办方所有。</p>
    <p>9.在法律允许范围内，主办方对本次活动拥有解释权。</p>

  </div>
        
</article>
    </div>

</div>
<?php include_once "part/footer.php"; ?>
<!--END #container-->
</body>
</html>
